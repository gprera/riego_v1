from flask import Flask, render_template, request
app = Flask(__name__)

# import RPi.GPIO as GPIO

# GPIO.setmode(GPIO.BCM)
# GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)

# @app.route('/')
# def home():
#     return render_template('home.html')

@app.route('/', methods=['GET','POST'])
def home():
    if request.method == 'POST':
        # GPIO.output(17, GPIO.HIGH)
        print 'method'
        start_date = request.form['start_date']
        print str(request.form.getlist('check'))

    return render_template('home.html')
    
@app.route('/about/')
def about():
    # GPIO.output(17, GPIO.LOW)
    return render_template('about.html')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
