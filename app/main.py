import RPi.GPIO as GPIO
import spidev
import time
import sys
import main2

spi = spidev.SpiDev()
spi.open(0, 0)
GPIO.setmode(GPIO.BCM)
# GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)

# @app.route('/')
# def home():
#     return render_template('home.html')



def buildReadCommand(channel):
    startBit = 0x01
    singleEnded = 0x08
    return [startBit, singleEnded | (channel << 4), 0]


def processAdcValue(result):
    byte2 = (result[1] & 0x03)
    return (byte2 << 8) | result[2]
    pass


def readAdc(channel):
    if ((channel > 7) or (channel < 0)):
        return -1
    r = spi.xfer2(buildReadCommand(channel))
    return processAdcValue(r)


if __name__ == '__main__':
    try:
        while True:
            val = readAdc(0)
            print "ADC Result: ", str(val)
            time.sleep(5)
    except KeyboardInterrupt:
        spi.close()
        sys.exit(0)
