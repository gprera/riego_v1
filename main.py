import RPi.GPIO as GPIO
import time
import datetime
import sys
import os
import logging
import logging.config
import config
import signal
from singleton import SingleInstance
import threading
import urllib2
menu_state = 0


def change_dir():
    """ Changes current working directory to where this file is stored so that imports work.
    """
    if getattr(sys, 'frozen', False):
        application_path = os.path.dirname(os.path.abspath(sys.executable))
    else:
        application_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(application_path)


def create_logger():
    """ Sets logging configuration and creates logger object
    :return: logger object
    """
    logging.config.fileConfig("loggers.config")
    return logging.getLogger('main')


def handler(signum, frame):
    logger.critical(" * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
    smart_app.log_events(2, 'Software End: Main loop STOP', '')
    smart_app.t_sync_stop = True
    time.sleep(5)
    logger.critical("HANDLER END SOFTWARE")
    logger.critical('Signal handler called with signal: [{0}-{1}]'.format(signum, frame))
    logger.critical(" * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")


def connect_to_wifi():
    try:
        os.system("sudo python wifi_ap_connect.py")
    except Exception as e:
        logger.critical("ERROR Wi-Fi connect: " + str(e))


def connect_to_ntp_server():
    try:
        logger.debug("Sync time to NTP Server")
        os.system("sudo /home/pi/mytimesync.sh")
        logger.debug("New system time.")
    except Exception as e:
        logger.critical("ERROR Wi-Fi connect: " + str(e))


def get_serial():
    # Extract serial from cpu info file
    cpu_serial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpu_serial = line[10:26]
        f.close()
    except Exception, msg:
        logger.error('Get serial number pc. {0}'.format(msg))
        cpu_serial = "ERROR000000000"
    logger.debug('Serial: {0}'.format(cpu_serial))
    return cpu_serial


def internet_on():
    try:
        urllib2.urlopen('http://www.google.com', timeout=1)
        return True
    except urllib2.URLError as err:
        logger.error("No INTERNET connection by: {0}".format(err))
        return False
    except Exception as err:
        logger.error("No INTERNET connection by: {0}".format(err))
        return False


# ----------------------------------------------------------------------------------------------------------------------
def loop():
    global menu_state
    smart_app.log_events(1, 'Software Init: Main loop START', '')
    continue_loop = True
    turn_off_lcd = 0
    lcd.lcd_clean_all()
    fact = datetime.datetime.now()
    lcd.lcd_string_centered(fact.strftime("%a %d/%b %H:%M"), 1)
    # ------------------------------------------------------------------------------------------------
    smart_app.sync_online()
    # ------------------------------------------------------------------------------------------------
    # MAIN LOOP
    while continue_loop:
        try:
            fact = datetime.datetime.now()
            # ----------------------------------------------------------------------------------------
            # LCD SCREEN
            if fact.second % 10 == 0 and fact.microsecond < 150000 and lcd.LCD_BACKLIGHT != 0:
                lcd.lcd_string_centered(fact.strftime("%a %d/%b %H:%M"), 1)
            # ----------------------------------------------------------------------------------------
            # SYNC On-line Database with Local Database -- Every 5 minutes with 20 seconds
            # if fact.minute % 5 == 0 and fact.second == 20 and fact.microsecond < 150000:
            #     sync_online(fact.minute == 35)
            # ----------------------------------------------------------------------------------------
            # CHECK INPUTS I2C (Rain Sensor), INPUTS ADC
            if fact.second == 30 and fact.microsecond < 450000:
                io.check_rain_sensor()
                io.check_adc_sensors()
                time.sleep(0.3)
            # ----------------------------------------------------------------------------------------
            # CHECK CONFIGURATION UPDATE -- Every 30 seconds
            if fact.second % 20 == 0 and fact.microsecond < 150000:
                try:
                    # smart_app.update_configuration()
                    # database.get_config()
                    # CHECK MANUAL MODE
                    if io.check_irrigation_now():
                        smart_app.update_sequences()
                        database.get_sequences()
                        io.irrigation_now_flag()
                        smart_app.t_sync_now = True
                    # CHECK MODES
                    io.check_mode()
                except Exception, msg:
                    logger.error("  ERROR with SmartApp connection by: {0}".format(msg))
            # ----------------------------------------------------------------------------------------
            # CLEAR RAIN SENSOR AT 5:00:00 AM
            if fact.hour == 5 and fact.minute == 0 and fact.second % 10 == 0 and fact.microsecond < 150000:
                io.clear_rain_sensor()
            # ----------------------------------------------------------------------------------------
            # CHECK BUTTONS PANEL (Menu, Ok)
            io.check_inputs_panel_buttons()
            # ----------------------------------------------------------------------------------------
            # LCD MANAGEMENT - OFF
            if not io.button_status[0] and not io.button_status[1]:
                if turn_off_lcd <= 100:
                    turn_off_lcd += 1
                if turn_off_lcd > 100 and lcd.LCD_BACKLIGHT != 0:
                    logger.debug('Turn-off LCD')
                    lcd.lcd_clean_all()
                    time.sleep(0.1)
                    lcd.lcd_backlight_off()
                time.sleep(0.1)
            # ----------------------------------------------------------------------------------------
            # BUTTON PRESSED: Turn-on LCD
            if io.button_status[0] or io.button_status[1]:
                turn_off_lcd = 0
                lcd.lcd_backlight_on()
                logger.debug('Turn-on LCD')
            # ----------------------------------------------------------------------------------------
            # BUTTON 1: Turn-on LCD
            if io.button_status[1]:
                logger.debug('Button B: pressed - only turn on LCD')
                fact = datetime.datetime.now()
                lcd.lcd_string_centered(fact.strftime("%a %d/%b %H:%M"), 1)
                io.button_status[1] = False
                time.sleep(0.1)
                # Wait for Menu button pressed
                io.wait_buttons_panel_press()
            # ----------------------------------------------------------------------------------------
            # BUTTON 0: Menu
            if io.button_status[0]:
                # Menu State 1:
                logger.debug('Button A: pressed - Menu')
                lcd.lcd_clean_all()
                lcd.lcd_string_centered('>  MENU  <', 1)
                io.button_status[0] = False
                menu_state = 0
                menu_gen()
                # . . . . . . . . . . . .
                logger.debug('Return to loop mode.')
                lcd.lcd_clean_all()
                fact = datetime.datetime.now()
                lcd.lcd_string_centered(fact.strftime("%a %d/%b %H:%M"), 1)
                logger.debug('Return to loop mode.  End loop')
        except Exception, msg:
            logger.error('ERROR on loop by: {0}'.format(msg))
            logger.critical("CRITICAL on loop")
            logger.critical(" * * * * * * * * * * * * * * * * * * * * * * ")
            logger.exception('Got exception on main handler')


# ----------------------------------------------------------------------------------------------------------------------

'''
def sync_online(with_connect):
    try:
        if not internet_on():
            return

        if with_connect:
            smart_app.is_connected = smart_app.connect_spreadsheet()
            smart_app.update_forecast()
        else:
            smart_app.update_app_sensors()

        if smart_app.check_connection():
            logger.debug("  Update information on Spreadsheet")
            smart_app.update_gardens_information()
            smart_app.update_irrigation_circuits()
            smart_app.update_sequences()
            smart_app.update_automatic_days()
            smart_app.update_modes()
            smart_app.update_configuration()
            # -----------------------------------------------
            database.get_garden_information()
            database.get_config()
            database.get_irrigation_circuits()
            database.get_sequences()
            database.get_irrigation_days()
        else:
            logger.error("   Can't connect with SmartApp (check internet connection)")
    except Exception, msg:
        logger.error("  ERROR with SmartApp connection by: {0}".format(msg))
'''


def confirm_sub_menu():
    # Wait button press
    io.wait_buttons_panel_press()
    # NOT PRESS BUTTON
    sub_menu_value = -1
    # OK
    if io.button_status[1]:
        logger.debug('Button B: pressed - Confirm')
        lcd.lcd_char('OK', 2, -3)
        time.sleep(1)
        sub_menu_value = 1
        io.button_status[1] = False
    # NEXT SUB-MENU
    elif io.button_status[0]:
        logger.debug('Button A: pressed - Next sub-menu')
        io.button_status[0] = False
        sub_menu_value = 0
    return sub_menu_value


def sub_menu_return():
    # **********************************************************************************************
    lcd.lcd_string_centered('Regresar', 2)  # Return to menu
    logger.debug('SUM-MENU: Return to menu')
    sub_menu_value = confirm_sub_menu()
    if sub_menu_value == -1:
        return -1
    if sub_menu_value == 1:
        lcd.lcd_clean_all()
        lcd.lcd_string_centered('>  MENU  <', 1)
        return 1
    # **********************************************************************************************
    lcd.lcd_string_centered('Salir', 2)  # Return to menu
    logger.debug('SUM-MENU: Exit menu')
    sub_menu_value = confirm_sub_menu()
    if sub_menu_value == -1 or sub_menu_value == 1:
        lcd.lcd_clean_all()
        return -1
    # **********************************************************************************************
    return 0


# ----------------------------------------------------------------------------------------------------------------------
# MENU:
#     > 0: MANUAL IRRIGATION SEQUENCE
#     > 1: MANUAL IRRIGATION CIRCUIT
#     > 2: SENSOR VALUES
#     > 3: CONFIGURATION - MODE OF IRRIGATION
#     > 4: CONFIGURATION - SENSORS (RAIN & MOISTURE)
#     > 5: CONFIGURATION - SOIL MOISTURE SENSOR LIMIT
#     > 6: CONFIGURATION - CANCEL IRRIGATION DAY
#     > 7: CONFIGURATION - CIRCUITS OF IRRIGATION
#     > 8: SEQUENCES - CREATE OR CONFIGURE IRRIGATION SEQUENCES  (tiempo de riego, seq01, seq02, seq03, seq04, seq05...)
#     > 9: AUTOMATIC DAYS - CREATE OR CONFIGURE IRRIGATION AUTOMATIC DAYS (dias, sequencia, horarios ...)
# ----------------------------------------------------------------------------------------------------------------------
#           '________________'___'
listMenu = ['Riego Manual    ',
            'Riego Individual',
            'Sensores        ',
            'Config Modo Riego',
            'Config Sensores ',
            'Config Hum.     ',
            'Cancelar Riego. ',
            'Circuitos Riego ',
            'Sequencias Riego',
            'Dias de Riego   ']


def menu_gen():
    global menu_state
    if (menu_state + 1) > len(listMenu):
        menu_state = 0
        logger.debug('** MENU NAVIGATION COMPLETE: EXIT **'.format())
        return

    logger.debug('** MENU NAVIGATION: {0} > {1} **'.format(menu_state, listMenu[menu_state]))
    lcd.lcd_string_left('*{0}'.format(listMenu[menu_state]), 2)
    sub_menu_value = confirm_sub_menu()
    if sub_menu_value == -1:
        return
    if sub_menu_value == 0:
        menu_state += 1
        menu_gen()
        return

    sub_menu_value = sub_menu_gen()
    if sub_menu_value == 1:
        menu_gen()


def sub_menu_gen():
    global menu_state
    if menu_state == 0:
        return sub_menu0()
    elif menu_state == 1:
        return sub_menu1()
    elif menu_state == 2:
        return sub_menu2()
    elif menu_state == 3:
        return sub_menu3()
    elif menu_state == 4:
        return sub_menu4()
    elif menu_state == 5:
        return sub_menu5()
    elif menu_state == 6:
        return sub_menu6()
    elif menu_state == 7:
        return sub_menu7()
    elif menu_state == 8:
        return sub_menu8()
    elif menu_state == 9:
        return sub_menu9()
    elif menu_state == 10:
        return sub_menu10()
    else:
        return -1


# ----------------------------------------------------------------------------------------------------------------------
# SUB-MENU 0: MANUAL IRRIGATION SEQUENCE
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu0():
    lcd.lcd_string_centered('RIEGO MANUAL', 1)
    logger.debug('MENU 0: Manual Irrigation by Sequences')
    # **********************************************************************************************
    for local_row in config.GARDEN_Irrigation_Sequences:
        logger.debug('   Sequence: {0} - Name: {1}'.format(local_row['id_sequence'], local_row['sequence_name']))
        lcd.lcd_string_left('*{0}'.format(local_row['sequence_name']), 2)

        sub_menu_value = confirm_sub_menu()

        if sub_menu_value == -1:
            return -1
        if sub_menu_value == 1:
            lcd.lcd_clean_line(2)
            lcd.lcd_string_centered('{0}'.format(local_row['sequence_name']), 1)
            lcd.lcd_string_centered('RIEGO MANUAL', 2)
            logger.debug('   > Select Irrigation Sequence {0}'.format(local_row['sequence_name']))
            time.sleep(2)
            database.update_manual_irrigation_on(local_row['id_sequence'])
            lcd.lcd_clean_line(2)
            return -1
    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu0()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# SUM-MENU 1: MANUAL IRRIGATION CIRCUIT
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu1():
    lcd.lcd_string_centered('RIEGO INDIVIDUAL', 1)
    logger.debug('MENU 1: Manual Irrigation by Circuits')
    # **********************************************************************************************
    for local_row in config.GARDEN_Irrigation_Circuits:
        logger.debug('   Circuit: {0} - Name: {1}'.format(local_row['id_circuit'], local_row['circuit_name']))
        lcd.lcd_string_left('*{0}'.format(local_row['circuit_name']), 2)

        sub_menu_value = confirm_sub_menu()
        if sub_menu_value == -1:
            return -1
        if sub_menu_value == 1:
            lcd.lcd_clean_line(2)
            lcd.lcd_string_centered('{0}'.format(local_row['circuit_name']), 1)
            lcd.lcd_string_centered('RIEGO MANUAL IND.', 2)
            logger.debug('   > Select Irrigation Circuit {0}'.format(local_row['circuit_name']))

            logger.debug('  - Valve: {0}'.format(local_row['id_circuit']))
            io.output_valves_i2c(local_row['id_circuit'], True)
            time.sleep(config.GARDEN_Irrigation_Circuits_Time)
            io.output_valves_i2c(local_row['id_circuit'], False)
            lcd.lcd_clean_line(2)
            return -1
    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu1()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# SUM-MENU 2: SENSORS VALUES (RAIN & SOIL MOISTURE)
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu2():
    lcd.lcd_string_centered('SENSORES', 1)
    logger.debug('MENU 2: Sensor values [Rain & Soil moisture)')
    # **********************************************************************************************
    lcd.lcd_string_centered('Lluvia Act: {0}'.format('SI' if config.SENSOR_Rain else 'NO'), 2)
    logger.debug('* Lluvia Act: {0}'.format('SI' if config.SENSOR_Rain else 'NO'))
    time.sleep(2)
    lcd.lcd_string_centered('Lluvia Dia: {0}'.format('SI' if config.SENSOR_Rain_Today else 'NO'), 2)
    logger.debug('* Lluvia Dia: {0}'.format('SI' if config.SENSOR_Rain_Today else 'NO'))
    time.sleep(2)
    lcd.lcd_string_centered('Humedad S1: {0:3}%'.format(config.SensorADC_Decode[0]), 2)
    logger.debug('* Humedad S1: {0}%'.format(config.SensorADC_Decode[0]))
    time.sleep(2)
    lcd.lcd_string_centered('Humedad S2: {0:3}%'.format(config.SensorADC_Decode[1]), 2)
    logger.debug('* Humedad S2: {0}%'.format(config.SensorADC_Decode[1]))
    time.sleep(2)
    lcd.lcd_string_centered('Humedad S3: {0:3}%'.format(config.SensorADC_Decode[2]), 2)
    logger.debug('* Humedad S3: {0}%'.format(config.SensorADC_Decode[2]))
    time.sleep(2)
    lcd.lcd_string_centered('Humedad S4: {0:3}%'.format(config.SensorADC_Decode[3]), 2)
    logger.debug('* Humedad S4: {0}%'.format(config.SensorADC_Decode[3]))
    time.sleep(2)
    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu2()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# SUM-MENU 3: CONFIGURATION - MODE OF IRRIGATION
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu3():
    lcd.lcd_string_centered('MODO DE RIEGO', 1)
    logger.debug('MENU 3: Configuration - Mode of Irrigation')
    # **********************************************************************************************
    for local_row in config.GENERAL_Modes:
        logger.debug('   Mode: {0} - Name: {1}'.format(local_row['id_mode'], local_row['mode_description']))
        lcd.lcd_string_left('*{0}'.format(local_row['lcd']), 2)
        if config.GARDEN_Config['id_mode'] == local_row['id_mode']:
            lcd.lcd_char('SI', 2, -3)
        else:
            lcd.lcd_char('NO', 2, -3)

        sub_menu_value = confirm_sub_menu()
        if sub_menu_value == -1:
            return -1
        if sub_menu_value == 1:
            logger.debug('   > Select New Configuration Mode {0}'.format(local_row['mode_description']))

            if database.save_mode(local_row['id_mode']):
                config.GARDEN_Config['id_mode'] = local_row['id_mode']
                lcd.lcd_char('SI', 2, -3)
            else:
                lcd.lcd_char('ERROR', 2, -5)
            time.sleep(2)
            break
    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu3()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# SUM-MENU 4: CONFIGURATION - SENSORS (RAIN & MOISTURE)
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu4():
    lcd.lcd_string_centered('CONFIG SENSORES', 1)
    logger.debug('MENU 4: Configuration - Sensor installed')
    # **********************************************************************************************

    value_sub_sensor = True if config.GARDEN_Config['rain_sensor'] == '1' else False
    logger.debug('   Mode: {0} - {1}'.format('Rain Sensor', value_sub_sensor))
    lcd.lcd_string_left('*{0}'.format('S. Lluvia: '), 2)
    lcd.lcd_char('SI' if value_sub_sensor else 'NO', 2, -3)
    sub_menu_value = confirm_sub_menu()
    if sub_menu_value == -1:
        return -1
    if sub_menu_value == 1:
        value_sub_sensor = not value_sub_sensor
        logger.debug('   > Change value to {0}'.format(value_sub_sensor))
        database.save_rain_sensor(value_sub_sensor)
        config.GARDEN_Config['rain_sensor'] = '1' if value_sub_sensor else '0'
        lcd.lcd_char('SI' if value_sub_sensor else 'NO', 2, -3)
        time.sleep(2)
    # ----------------------------------------------------------------------------------------------
    value_sub_sensor = True if config.GARDEN_Config['moisture_sensor'] == '1' else False
    logger.debug('   Mode: {0} - {1}'.format('Soil moisture Sensor', value_sub_sensor))
    lcd.lcd_string_left('*{0}'.format('S. Humedad: '), 2)
    lcd.lcd_char('SI' if value_sub_sensor else 'NO', 2, -3)
    sub_menu_value = confirm_sub_menu()
    if sub_menu_value == -1:
        return -1
    if sub_menu_value == 1:
        value_sub_sensor = not value_sub_sensor
        logger.debug('   > Change value to {0}'.format(value_sub_sensor))
        database.save_moisture_sensor(value_sub_sensor)
        config.GARDEN_Config['moisture_sensor'] = '1' if value_sub_sensor else '0'
        lcd.lcd_char('SI' if value_sub_sensor else 'NO', 2, -3)
        time.sleep(2)

    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu4()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# SUM-MENU 5: CONFIGURATION - SOIL MOISTURE SENSOR LIMIT
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu5():
    lcd.lcd_string_centered('LIMITE DE HUMEDAD', 1)
    logger.debug('MENU 5: Configuration of Soil moisture sensor limit')
    # **********************************************************************************************
    for i in range(0, 10):
        s1_level = (i * 5) + 25
        logger.debug('   Moisture limit: {0:3}%'.format(s1_level))
        lcd.lcd_string_left('* Limite {0:3}'.format(s1_level), 2)
        lcd.lcd_char('SI' if config.GARDEN_Config['humidity_level'] == s1_level else 'NO', 2, -3)
        sub_menu_value = confirm_sub_menu()
        if sub_menu_value == -1:
            return -1
        if sub_menu_value == 1:
            logger.debug('   > Change value to {0}'.format(s1_level))
            database.save_moisture_sensor_limit(s1_level)
            config.GARDEN_Config['humidity_level'] = s1_level
            lcd.lcd_char('SI' if config.GARDEN_Config['humidity_level'] == s1_level else 'NO', 2, -3)
            time.sleep(2)
            break
    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu5()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# SUM-MENU 6: CONFIGURATION - CANCEL IRRIGATION DAY
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu6():
    lcd.lcd_string_centered('CANCELAR DIA RIEGO', 1)
    logger.debug('MENU 6: Cancel irrigation day')
    # **********************************************************************************************

    value_sub_sensor = True if config.GARDEN_Config['irrigation_cancel'] == '1' else False
    logger.debug('   Mode: {0} - {1}'.format('Irrigation Cancel Day', value_sub_sensor))
    lcd.lcd_string_left('*{0}'.format('R. Cancelado: '), 2)
    lcd.lcd_char('SI' if value_sub_sensor else 'NO', 2, -3)
    if value_sub_sensor:
        time.sleep(2)
        lcd.lcd_string_left("SI: {0:%d/%m/%Y}".format(config.GARDEN_Config['irrigation_cancel_day']), 2)
        time.sleep(2)
        lcd.lcd_string_left('*{0}'.format('R. Cancelado: '), 2)
        lcd.lcd_char('SI' if value_sub_sensor else 'NO', 2, -3)

    sub_menu_value = confirm_sub_menu()
    if sub_menu_value == -1:
        return -1
    if sub_menu_value == 1:
        value_sub_sensor = not value_sub_sensor
        logger.debug('   > Change value to {0}'.format(value_sub_sensor))

        d_val = int("{0:%d}".format(datetime.datetime.now()))
        m_val = int("{0:%m}".format(datetime.datetime.now()))
        y_val = int("{0:%Y}".format(datetime.datetime.now()))

        if value_sub_sensor:
            x = 0
            while x < 3:
                # Ask for new date to cancel
                if x == 0:
                    lcd.lcd_string_left(" Dia:  {0}/{1}/{2}".format(d_val, m_val, y_val), 2)
                elif x == 1:
                    lcd.lcd_string_left(" Mes:  {0}/{1}/{2}".format(d_val, m_val, y_val), 2)
                elif x == 2:
                    lcd.lcd_string_left(" A~o:  {0}/{1}/{2}".format(d_val, m_val, y_val), 2)
                sub_menu_value = confirm_sub_menu()
                if sub_menu_value == -1:
                    return -1
                if sub_menu_value == 1:
                    x += 1
                    continue
                else:
                    if x == 0:
                        d_val += 1
                    elif x == 1:
                        m_val += 1
                    elif x == 2:
                        y_val += 1

        database.save_irrigation_cancel(value_sub_sensor, "{0}/{1}/{2}".format(d_val, m_val, y_val))
        config.GARDEN_Config['irrigation_cancel'] = '1' if value_sub_sensor else '0'
        lcd.lcd_string_left('*{0}'.format('R. Cancelado: '), 2)
        lcd.lcd_char('SI' if value_sub_sensor else 'NO', 2, -3)
        time.sleep(2)
    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu6()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# MENU 7: CONFIGURATION - CIRCUITS OF IRRIGATION
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu7():
    lcd.lcd_string_centered('CIRCUITOS RIEGO', 1)
    logger.debug('MENU 7: Irrigation Circuits - Information')
    # **********************************************************************************************
    for local_row in config.IRRIGATION_Circuits:
        logger.debug('   Number: {0} - Name: {1}'.format(local_row['id_circuit'], local_row['circuit_name']))
        lcd.lcd_string_left('{0}:{1}'.format(local_row['id_circuit'], local_row['circuit_name']), 2)
        time.sleep(2.5)
    # **********************************************************************************************
    continue_exit_menu = sub_menu_return()
    if continue_exit_menu != 0:
        return continue_exit_menu
    # **********************************************************************************************
    return sub_menu7()
    # ------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# MENU 8: CREATE OR CONFIGURE IRRIGATION SEQUENCES
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu8():
    # pasar por todas las sequencias
    # la ultima es NUEVA, por si se quiere crear una sequencia.
    pass


# ----------------------------------------------------------------------------------------------------------------------
# MENU 9: CREATE OR CONFIGURE IRRIGATION SEQUENCES
# ----------------------------------------------------------------------------------------------------------------------
def sub_menu9():
    # pasar por todas las sequencias
    # la ultima es NUEVA, por si se quiere crear una sequencia.
    pass


# ----------------------------------------------------------------------------------------------------------------------
# MAIN
# ----------------------------------------------------------------------------------------------------------------------
signal.signal(signal.SIGABRT, handler)
signal.signal(signal.SIGTERM, handler)
# signal.signal(signal.SIGINT, signal_handler)

if __name__ == '__main__':
    change_dir()
    logger = create_logger()
    logger.info('*' * 50)
    logger.info('Garden Irrigation System Software Start')
    connect_to_wifi()
    single = SingleInstance()
    try:
        # ----------------------------------------------------------------------------------------------
        connect_to_ntp_server()
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: LCD display')
        if config.USE_LCD:
            import lcd as lcd
        else:
            import lcd_dummy as lcd
        lcd.initialize()
        lcd.lcd_string_centered("Sistema de Riego", 1)
        lcd.lcd_string_centered("Inteligente", 2)
        time.sleep(2)
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: Database (local)')
        import database
        lcd.lcd_string_left("> Database: ", 2)
        database.connect(config.DB_CONN_STR)
        lcd.lcd_char('OK', 2, -4)
        time.sleep(1.5)
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: SmartApp (on-line database)')
        import smart_app
        lcd.lcd_string_left("> SmartApp: ", 2)
        if smart_app.connect_spreadsheet():
            logger.debug('   - Garden information')
            smart_app.update_gardens_information()
            logger.debug('   - Garden irrigation circuits')
            smart_app.update_irrigation_circuits()
            logger.debug('   - Garden irrigation sequences')
            smart_app.update_sequences()
            logger.debug('   - Garden irrigation automatic days')
            smart_app.update_automatic_days()
            logger.debug('   - Garden mode of operation')
            smart_app.update_modes()
            logger.debug('   - Garden configuration')
            smart_app.update_configuration()
            logger.debug('   - Update forecast')
            smart_app.update_forecast()
            lcd.lcd_char('OK', 2, -4)
        else:
            logger.error("   Can't connect with SmartApp (check internet connection)")
            lcd.lcd_char('BAD', 2, -4)
        time.sleep(1.5)
        # ----------------------------------------------------------------------------------------------
        lcd.lcd_string_left("> Configuration: ", 2)
        logger.debug(' > Connecting with: Database (Get garden information)')
        database.get_garden_information()
        logger.debug('   System Information: ')
        logger.debug('   * ID Garden:     {0}'.format(config.GARDEN_Info['id_garden']))
        logger.debug('   * Serial:        {0}'.format(config.GARDEN_Info['serial']))
        logger.debug('   * Garden Name:   {0}'.format(config.GARDEN_Info['garden_name']))
        logger.debug('   * LCD Display:   {0}'.format(config.GARDEN_Info['lcd']))
        logger.debug('   * User:          {0}'.format(config.GARDEN_Info['user_mail']))
        logger.debug('   * Enabled:       {0}'.format(config.GARDEN_Info['enabled']))
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: Database (Get configuration)')
        database.get_config()
        logger.debug('   System Configuration: ')
        logger.debug('   * ID Config:       {0}'.format(config.GARDEN_Config['id_config']))
        logger.debug('   * ID Mode:         {0}'.format(config.GARDEN_Config['id_mode']))
        logger.debug('   * Mode:            {0}'.format(config.GARDEN_Config['mode_description']))
        logger.debug('   * Mode LCD:        {0}'.format(config.GARDEN_Config['mode_lcd']))
        logger.debug('   * Rain Sensor:     {0}'.format(config.GARDEN_Config['rain_sensor']))
        logger.debug('   * Moisture Sensor: {0}'.format(config.GARDEN_Config['moisture_sensor']))
        logger.debug('   * Humidity Level:  {0}'.format(config.GARDEN_Config['humidity_level']))
        logger.debug('   * Irr. Cancel:     {0}'.format(config.GARDEN_Config['irrigation_cancel']))
        logger.debug('   * Irr. Cancel Day: {0}'.format(config.GARDEN_Config['irrigation_cancel_day']))
        logger.debug('   * Irrigation Now:  {0}'.format(config.GARDEN_Config['irrigation_now']))
        logger.debug('   * Default Irr Seq: {0}'.format(config.GARDEN_Config['default_irrigation_sequence']))
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: Database (Get irrigation circuits)')
        database.get_irrigation_circuits()
        logger.debug('   Irrigation Circuits: ')
        for row in config.GARDEN_Irrigation_Circuits:
            logger.debug('   * {0}'.format(row))
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: Database (Get irrigation sequences)')
        database.get_sequences()
        logger.debug('   Irrigation Sequences: ')
        for row in config.GARDEN_Irrigation_Sequences:
            logger.debug('   * {0}'.format(row))
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: Database (Get irrigation automatic days)')
        database.get_irrigation_days()
        logger.debug('   Irrigation Automatic Days: ')
        for row in config.GARDEN_Irrigation_Days:
            logger.debug('   * {0}'.format(row))
        # ----------------------------------------------------------------------------------------------
        lcd.lcd_char('OK', 2, -4)
        time.sleep(1.5)
        # ----------------------------------------------------------------------------------------------
        database.log_event('1', 'Software Init: Smart Irrigation System', '')
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Get Serial Number from main board')
        lcd.lcd_string_left("> Serial No: ", 2)
        serialNo = get_serial()
        serialNoDB = config.GARDEN_Info['serial']
        time.sleep(0.5)
        if not(serialNo == serialNoDB):
            raise ValueError('Serial numbers do not match')
        else:
            lcd.lcd_char('OK', 2, -4)
        time.sleep(1)
        # ----------------------------------------------------------------------------------------------
        lcd.lcd_string_left("> Mode Config: ", 2)
        time.sleep(1)
        lcd.lcd_string_left(" * {0}".format(config.GARDEN_Config['mode_lcd']), 2)
        time.sleep(1.5)
        # ----------------------------------------------------------------------------------------------
        logger.debug(' > Connecting with: Inputs/Outputs')
        lcd.lcd_string_left(" > I/O Board: ", 2)
        import in_out_i2c as io
        io.connect()
        lcd.lcd_char('OK', 2, -4)
        time.sleep(1.5)
        # ----------------------------------------------------------------------------------------------
    except KeyboardInterrupt:
        lcd.lcd_char('BAD', 2, -4)
        time.sleep(2)
        sys.exit(0)
    else:
        # t_main = threading.Thread(target=loop)
        # t_main.start()
        loop()


