import yaml
import os
import inspect

currdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
with file(os.path.join(currdir, 'config.txt'), 'r') as f:
    variables = yaml.load(f)


# LOCAL DATABASE
DB_SERVER = 'localhost'
DB_NAME = 'db_riego'
# DB_USER = 'postgres' 'apassword'
DB_USER = 'release'
DB_CONN_STR = "host='{0}' dbname='{1}' user='{2}' password='{3}'".format(DB_SERVER, DB_NAME, DB_USER, 'release')
DB_SERIAL = ''

# FORECAST
Latitude = variables['LATITUDE']  # 14.530571
Longitude = variables['LONGITUDE']  # 90.448887

# GARDENS
GARDEN_Id = '0'
GARDEN_Serial = '0'
GARDEN_Name = 'Smart Garden'
GARDEN_LCD = 'Smart Garden'
GARDEN_User = "smartgarden@gmail.com"
GARDEN_Active = True
GARDEN_Edited = None

# IRRIGATION CIRCUITS
IRRIGATION_Circuits = []
IRRIGATION_Sequences = []
IRRIGATION_Days = []

#
GENERAL_Modes = []
GENERAL_Configuration = []
# GENERAL CONFIG
GENERAL_NombreJardin = 'Jardin'
GENERAL_ModoActual = 1
GENERAL_Estado = 0
GENERAL_TiempoRiego = 30
GENERAL_SensorHumedad = 40

# SEQUENCES
IRRIGATION_Gardens = None

# GENERAL
GARDEN_Info = []
GARDEN_Config = []
GARDEN_Irrigation_Sequences = []
GARDEN_Irrigation_Circuits = []
GARDEN_Irrigation_Days = []
GARDEN_Irrigation_Circuits_Time = 15


GARDEN_ID_Local = variables['JARDIN ID']  # int
# STATION_TYPE = variables['STATION TYPE'].upper()  # 'D' or 'V'
# STATION_FAMILY = variables['STATION FAMILY'].upper()
# LANGUAGE = variables['LANGUAGE'].lower()


# LCD VARIABLES
USE_LCD = variables['LCD']
LCD_ADD = variables['LCD_Address']

# SENSORS
SensorADC = [0] * 4
SensorADC_Decode = [0] * 4
SENSOR_Rain = False
SENSOR_Rain_Today = False

# COMMUNICATION
WIFI_CONNECT = variables['WIFI_CONNECT'] # True or False
SSID = variables['SSID']  # string
SSID_PASS = variables['SSID_PASS']  # password
DHCP_IP = variables['DHCP_IP'] # True or False

IP = 'DHCP' if not 'IP' in variables else variables['IP']  # str [192.168.1.1]
PC_IP = variables['PC IP']  # str [192.168.1.1]
PORT = variables['PORT']  # int [50008]
GATEWAY = '' if not 'GATEWAY' in variables else variables['GATEWAY']
