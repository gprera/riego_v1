import logging
import logging.config
import config
from datetime import datetime
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from unidecode import unidecode
import database
import forecast
# -------------------
import requests
import threading
# -------------------
import random
import string
# -------------------
import urllib2


logger = logging.getLogger(__name__)
logger_main = logging.getLogger('main.{0}'.format(__name__))


is_connected = False
scope = None
credentials = None
gc = None
wks = None


# ----------------------------------------------------------------------------------------------------------------------
# CHECK INTERNET CONNECTION
def internet_on():
    try:
        urllib2.urlopen('http://www.google.com', timeout=1)
        return True
    except urllib2.URLError as err:
        logger.error("No INTERNET connection by: {0}".format(err))
        return False
    except Exception:
        logger.error("No INTERNET connection by: {0}".format(err))
        return False
# ----------------------------------------------------------------------------------------------------------------------


def log_file(type, msg, log_main=False):
    if type == 'debug':
        logger.debug(msg)
    if type == 'info':
        logger.info(msg)
    if type == 'critical':
        logger.critial(msg)
    if type == 'error':
        logger.error(msg)
    if type == 'error':
        logger.exception(msg)

    if log_main:
        if type == 'debug':
            logger_main.debug(msg)
        if type == 'info':
            logger_main.info(msg)
        if type == 'critical':
            logger_main.critial(msg)
        if type == 'error':
            logger_main.error(msg)
        if type == 'error':
            logger_main.exception(msg)


def connect_spreadsheet():
    global wks, scope, credentials, gc, is_connected
    try:
        log_file('info', '   - Spreadsheet Connecting to on-line database', True)
        scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        credentials = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', scope)
        gc = gspread.authorize(credentials)
        wks = gc.open('SmartGardens')
        is_connected = True
        return True
    except Exception, msg:
        log_file('error', "   ? Connecting to on-line spreadsheet: " + str(msg), True)
        return False


def check_connection():
    global wks, scope, credentials, gc, is_connected
    try:
        wks = gc.open('SmartGardens')
        is_connected = True
    except Exception, msg:
        logger.error("   ? Check Connection to on-line spreadsheet: " + str(msg))
        is_connected = connect_spreadsheet()
    finally:
        return is_connected


# ----------------------------------------------------------------------------------------------------------------------
# GET GARDEN ID CONFIGURATION ON SPREADSHEET
def update_gardens_information():
    global wks, scope, credentials, gc, is_connected
    try:
        logger.debug('   - Gardens information')
        f_garden = wks.worksheet('Gardens')
        value_list = f_garden.row_values(1)
        list_of_lists = f_garden.get_all_values()

        col_serial = value_list.index('Serial')
        col_garden = value_list.index('Garden Name')
        col_lcd = value_list.index('LCD')
        col_user = value_list.index('User')
        col_active = value_list.index('Enabled')
        col_edited = value_list.index('Edited')

        row_id = None
        row_counter = 0
        for val_row in list_of_lists:
            if val_row[value_list.index('ID Garden')] == str(config.GARDEN_ID_Local):
                row_id = val_row
                break
            row_counter += 1

        if row_id is None:
            logger.error("  ? Garden Information is not provided in the Online Database")

        db_ok = database.get_gardens_info()
        # NEEDS UPDATE
        dt_remote = datetime.strptime(row_id[col_edited], "%d/%m/%Y %H:%M:%S")

        if not db_ok:
            config.GARDEN_Serial = row_id[col_serial]
            config.GARDEN_Name = row_id[col_garden]
            config.GARDEN_LCD = row_id[col_lcd]
            config.GARDEN_User = row_id[col_user]
            config.GARDEN_Active = str(int(row_id[col_active] == 'TRUE'))
            config.GARDEN_Edited = dt_remote
            database.update_gardens_info()
            logger.info("Local copy is empty >> Online copy is newer - [Local copy is now updated]")
            database.get_gardens_info()

        dt_local = config.GARDEN_Edited.replace(microsecond=0)
        # logger.debug("Garden local last edition:  {0}".format(dt_local))

        if dt_remote == dt_local:
            logger.info("Both copies are updated")
            pass
        elif dt_remote > dt_local:
            config.GARDEN_Serial = row_id[col_serial]
            config.GARDEN_Name = row_id[col_garden]
            config.GARDEN_LCD = row_id[col_lcd]
            config.GARDEN_User = row_id[col_user]
            config.GARDEN_Active = str(int(row_id[col_active] == 'TRUE'))
            config.GARDEN_Edited = dt_remote
            database.update_gardens_info()
            logger.info("Online copy is newer - [Local copy is now updated]")
        else:
            f_garden.update_cell(row_counter + 1, col_serial + 1, config.GARDEN_Serial)
            f_garden.update_cell(row_counter + 1, col_garden + 1, config.GARDEN_Name)
            f_garden.update_cell(row_counter + 1, col_lcd + 1, config.GARDEN_LCD)
            f_garden.update_cell(row_counter + 1, col_user + 1, config.GARDEN_User)
            f_garden.update_cell(row_counter + 1, col_active + 1, str(config.GARDEN_Active == '1').upper())
            f_garden.update_cell(row_counter + 1, col_edited + 1, config.GARDEN_Edited.strftime("%d/%m/%Y %H:%M:%S"))
            logger.info("Local copy is newer - [Online copy is now updated]")
    except Exception, msg:
        is_connected = False
        logger.error('Error while updating spreadsheet Gardens_Information: {0}'.format(msg))
# ----------------------------------------------------------------------------------------------------------------------


def update_appsheet_gardens():
    # save data on Spreadsheet
    try:
        a = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(12))
        url = "https://api.appsheet.com/api/v1/apps/6a9a6990-6776-4442-a9b1-977e250a3e96/tables/Gardens"
        payload = ("{" 
                   "\"Action\": \"Edit\","
                   "\"Properties\": {"
                   "    \"Locale\": \"en-US\","
                   "    \"Timezone\": \"Central America Standard Time\"," 
                   "    \"runAsUserEmail\": \"gprera@gmail.com\""
                   "},"
                   "\"Rows\": ["
                   "{"
                   "\"ID Garden\": \"" + str(config.GARDEN_Id) + "\","
                   "\"Serial\": \"" + str(config.GARDEN_Serial) + "\", "
                   "\"Garden Name\": \"" + str(config.GARDEN_Name) + "\","
                   "\"LCD\": \"" + str(config.GARDEN_LCD) + "\","
                   "\"User\": \"" + str(config.GARDEN_User) + "\","
                   "\"Enabled\": \"" + str(config.GARDEN_Active) + "\","
                   "\"Edited\": \"" + config.GARDEN_Edited.strftime("%m/%d/%Y %H:%M:%S") + "\""
                   "}"
                   "]"
                   "}"
                   "")
        headers = {
            'ApplicationAccessKey': "itHUQ-k7Nnl-Uctqz-pXgHa-70xs9-K2h0w-2swRh-lZ13P",
            'Content-Type': "application/json",
            'cache-control': "no-cache",
            'Postman-Token': "d1df78be-e60f-45f8-8e22-" + a
        }
        response = requests.request("POST", url, data=payload, headers=headers)
        logger.debug("Update Gardens: [Success: {0}]".format(response.json().get('Success')))
        if not response.json().get('Success'):
            logger.debug('   - Update Gardens Payload:  {0}'.format(payload))
            logger.debug('   - Update Gardens Response: {0}'.format(response.text))
    except Exception, msg:
        logger.error("   ? POST Error: " + str(msg))
        pass


def get_appsheet_table(table):
    try:
        url = "https://api.appsheet.com/api/v2/apps/6a9a6990-6776-4442-a9b1-977e250a3e96/tables/" + table + "/Action"
        logger.debug("User: {0}".format(config.GARDEN_User))
        if config.GARDEN_User == 'smartgarden@gmail.com':
            config.GARDEN_User = 'mmsalazaralonso@gmail.com'
        logger.debug("User: {0}".format(config.GARDEN_User))

        random_header = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(12))
        headers = {
            'ApplicationAccessKey': "itHUQ-k7Nnl-Uctqz-pXgHa-70xs9-K2h0w-2swRh-lZ13P",
            'Content-Type': "application/json",
            'cache-control': "no-cache",
            'Postman-Token': "05c9e6b4-65ca-4c31-9ce2-" + random_header
        }
        payload = "{" \
                  "\"Action\": \"Find\"," \
                  "\"Properties\": {" \
                  "    \"Locale\": \"en-US\"," \
                  "    \"Timezone\": \"Central America Standard Time\"," \
                  "    \"runAsUserEmail\": \"" + str(config.GARDEN_User) + "\"" \
                  "}," \
                  "\"Rows\": [" \
                  "    {" \
                  "        \"ID Garden\": " + str(config.GARDEN_ID_Local) + \
                  "    }" \
                  "]" \
                  "}"
        # + str(config.GARDEN_ID_Local) + \
        response = requests.request("POST", url, data=payload, headers=headers)
        return response.json()
        #print(response.text)
        #print(response.json())
        #print(len(response.json()))
        #print("Log Events Response: [Success: {0}]".format(response.json()[0].get('_RowNumber')))
        # print("Log Events Response: [Success: {0}]".format(response.json().get('RowNumber')))
    except Exception, msg:
        logger.error("   ? POST Error: " + str(msg))
        raise

# ----------------------------------------------------------------------------------------------------------------------
# GET GARDEN IRRIGATION CIRCUITS ON SPREADSHEET
def update_irrigation_circuits():
    global wks, scope, credentials, gc
    logger.debug('   - Gardens irrigation circuits')
    f_circuits = wks.worksheet('Irrigation_Circuits')
    value_list = f_circuits.row_values(1)
    list_of_lists = f_circuits.get_all_records()

    col_name = value_list.index('Circuit Name') + 1
    col_sprinkles = value_list.index('Sprinkles') + 1
    col_edited = value_list.index('Edited') + 1

    database.irrigation_circuits_get()

    row_counter = 1
    for online_row in list_of_lists:
        found_circuit = False
        row_counter += 1
        if str(online_row['ID Garden']) != str(config.GARDEN_ID_Local):
            continue
        for local_row in config.IRRIGATION_Circuits:
            if online_row['ID Circuit'] == local_row['id_circuit']:
                found_circuit = True
                dt_remote = datetime.strptime(online_row['Edited'], "%d/%m/%Y %H:%M:%S")
                dt_local = local_row['edited'].replace(microsecond=0)
                if dt_remote == dt_local:
                    logger.debug("Circuit {0} - Both copies are updated".format(local_row['id_circuit']))
                    pass
                elif dt_remote > dt_local:
                    database.irrigation_circuits_update(online_row)
                    logger.info("Online copy is newer - [Local copy is now updated]")
                else:
                    f_circuits.update_cell(row_counter, col_name, local_row['circuit_name'])
                    f_circuits.update_cell(row_counter, col_sprinkles, local_row['sprinkles'])
                    f_circuits.update_cell(row_counter, col_edited, local_row['edited'].strftime("%d/%m/%Y %H:%M:%S"))
                    # logger.info("Local copy is newer - [Online copy is now updated]")
        if not found_circuit:
            database.irrigation_circuits_insert(online_row)
            logger.info("Circuit NOT found on Local copy - [Local copy is now updated]")
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# GET GARDEN IRRIGATION SEQUENCES SPREADSHEET
def update_sequences():
    global wks, scope, credentials, gc
    logger.debug('   - Gardens irrigation sequences')
    f_sequences = wks.worksheet('Sequences')
    value_list = f_sequences.row_values(1)
    list_of_lists = f_sequences.get_all_records()

    col_name = value_list.index('Sequence Name') + 1
    col_time = value_list.index('Irrigation Time') + 1
    col_edited = value_list.index('Edited') + 1
    col_now = value_list.index('Now') + 1

    database.sequences_get()

    row_counter = 1
    for online_row in list_of_lists:
        found_sequence = False
        row_counter += 1
        if str(online_row['ID Garden']) != str(config.GARDEN_ID_Local):
            continue
        for local_row in config.IRRIGATION_Sequences:
            if online_row['ID Sequence'] == local_row['id_sequence']:
                found_sequence = True
                dt_remote = datetime.strptime(online_row['Edited'], "%d/%m/%Y %H:%M:%S")
                dt_local = local_row['edited'].replace(microsecond=0)
                if dt_remote == dt_local:
                    logger.debug("Circuit {0} - Both copies are updated".format(local_row['id_sequence']))
                    pass
                elif dt_remote > dt_local:
                    database.sequences_update(online_row)
                    database.sequences_get()
                    logger.info("Online copy is newer - [Local copy is now updated]")
                else:
                    f_sequences.update_cell(row_counter, col_name, local_row['sequence_name'])
                    f_sequences.update_cell(row_counter, col_time, local_row['irrigation_time'].strftime("%H:%M:%S"))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_01')+1,
                                            '' if local_row['seq_01'] is None else
                                            ','.join(str(x) for x in local_row['seq_01']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_02')+1,
                                            '' if local_row['seq_02'] is None else
                                            ','.join(str(x) for x in local_row['seq_02']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_03')+1,
                                            '' if local_row['seq_03'] is None else
                                            ','.join(str(x) for x in local_row['seq_03']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_04')+1,
                                            '' if local_row['seq_04'] is None else
                                            ','.join(str(x) for x in local_row['seq_04']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_05')+1,
                                            '' if local_row['seq_05'] is None else
                                            ','.join(str(x) for x in local_row['seq_05']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_06')+1,
                                            '' if local_row['seq_06'] is None else
                                            ','.join(str(x) for x in local_row['seq_06']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_07')+1,
                                            '' if local_row['seq_07'] is None else
                                            ','.join(str(x) for x in local_row['seq_07']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_08')+1,
                                            '' if local_row['seq_08'] is None else
                                            ','.join(str(x) for x in local_row['seq_08']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_09')+1,
                                            '' if local_row['seq_09'] is None else
                                            ','.join(str(x) for x in local_row['seq_09']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_10')+1,
                                            '' if local_row['seq_10'] is None else
                                            ','.join(str(x) for x in local_row['seq_10']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_11')+1,
                                            '' if local_row['seq_11'] is None else
                                            ','.join(str(x) for x in local_row['seq_11']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_12')+1,
                                            '' if local_row['seq_12'] is None else
                                            ','.join(str(x) for x in local_row['seq_12']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_13')+1,
                                            '' if local_row['seq_13'] is None else
                                            ','.join(str(x) for x in local_row['seq_13']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_14')+1,
                                            '' if local_row['seq_14'] is None else
                                            ','.join(str(x) for x in local_row['seq_14']))
                    f_sequences.update_cell(row_counter, value_list.index('Seq_15')+1,
                                            '' if local_row['seq_15'] is None else
                                            ','.join(str(x) for x in local_row['seq_15']))
                    f_sequences.update_cell(row_counter, col_edited, local_row['edited'].strftime("%d/%m/%Y %H:%M:%S"))
                    f_sequences.update_cell(row_counter, col_now, str(local_row['irrigation_now'] == '1').upper())
                    logger.info("Local copy is newer - [Online copy is now updated]")
        if not found_sequence:
            database.sequences_insert(online_row)
            database.sequences_get()
            logger.info("Circuit NOT found on Local copy - [Local copy is now updated]")
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# GET GARDEN IRRIGATION SEQUENCES SPREADSHEET
def update_automatic_days():
    global wks, scope, credentials, gc
    logger.debug('   - Gardens irrigation automatic days')
    f_days = wks.worksheet('Automatic_Days')
    value_list = f_days.row_values(1)
    list_of_lists = f_days.get_all_records()

    col_name = value_list.index('Days Name') + 1
    col_sequence = value_list.index('ID Sequence') + 1
    col_edited = value_list.index('Edited') + 1

    database.automatic_days_get()

    row_counter = 1
    for online_row in list_of_lists:
        found_circuit = False
        row_counter += 1
        if str(online_row['ID Garden']) != str(config.GARDEN_ID_Local):
            continue
        for local_row in config.IRRIGATION_Days:
            if online_row['ID Days'] == local_row['id_days']:
                found_circuit = True
                dt_remote = datetime.strptime(online_row['Edited'], "%d/%m/%Y %H:%M:%S")
                dt_local = local_row['edited'].replace(microsecond=0)
                if dt_remote == dt_local:
                    logger.debug("Automatic_Days {0} - Both copies are updated".format(local_row['id_days']))
                    pass
                elif dt_remote > dt_local:
                    database.automatic_days_update(online_row)
                    database.automatic_days_get()
                    logger.info("Automatic_Days {0} - Online copy is newer - [Local copy is now updated]")
                else:
                    f_days.update_cell(row_counter, col_sequence, local_row['id_sequence'])
                    f_days.update_cell(row_counter, col_name, local_row['days_name'])
                    f_days.update_cell(row_counter, value_list.index('Monday') + 1,
                                       '' if local_row['monday'] is None else
                                       local_row['monday'].strftime("%H:%M:%S"))
                    f_days.update_cell(row_counter, value_list.index('Tuesday') + 1,
                                       '' if local_row['tuesday'] is None else
                                       local_row['tuesday'].strftime("%H:%M:%S"))
                    f_days.update_cell(row_counter, value_list.index('Wednesday') + 1,
                                       '' if local_row['wednesday'] is None else
                                       local_row['wednesday'].strftime("%H:%M:%S"))
                    f_days.update_cell(row_counter, value_list.index('Thursday') + 1,
                                       '' if local_row['thursday'] is None else
                                       local_row['thursday'].strftime("%H:%M:%S"))
                    f_days.update_cell(row_counter, value_list.index('Friday') + 1,
                                       '' if local_row['friday'] is None else
                                       local_row['friday'].strftime("%H:%M:%S"))
                    f_days.update_cell(row_counter, value_list.index('Saturday') + 1,
                                       '' if local_row['saturday'] is None else
                                       local_row['saturday'].strftime("%H:%M:%S"))
                    f_days.update_cell(row_counter, value_list.index('Sunday') + 1,
                                       '' if local_row['sunday'] is None else
                                       local_row['sunday'].strftime("%H:%M:%S"))
                    f_days.update_cell(row_counter, col_edited, local_row['edited'].strftime("%d/%m/%Y %H:%M:%S"))
                    logger.info("Automatic_Days {0} - Local copy is newer - [Online copy is now updated]".format(
                        local_row['id_days']))
        if not found_circuit:
            database.automatic_days_insert(online_row)
            database.automatic_days_get()
            logger.info("Automatic_Days {0} - Automatic day NOT found on Local copy - "
                        "[Local copy is now updated]".format(online_row['ID Days']))
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# GET GARDEN IRRIGATION SEQUENCES SPREADSHEET
def update_modes():
    global wks, scope, credentials, gc
    logger.debug('   - Garden mode of operation')
    f_modes = wks.worksheet('Mode')
    list_of_lists = f_modes.get_all_records()

    database.modes_get()

    row_counter = 1
    for online_row in list_of_lists:
        found_row = False
        row_counter += 1
        for local_row in config.GENERAL_Modes:
            if online_row['ID Mode'] == local_row['id_mode']:
                found_row = True
                if (unidecode(online_row['Mode Description']) == local_row['mode_description'] and
                   unidecode(online_row['LCD']) == local_row['lcd']):
                    logger.debug("Mode {0} - Both copies are updated".format(local_row['id_mode']))
                    pass
                else:
                    online_row['LCD'] = unidecode(online_row['LCD'])
                    online_row['Mode Description'] = unidecode(online_row['Mode Description'])
                    database.modes_update(online_row)
                    database.modes_get()
                    logger.info("Mode {0} - Online copy is newer - [Local copy is now updated]".format(
                        local_row['id_mode']))
        if not found_row:
            database.modes_insert(online_row)
            database.modes_get()
            logger.info("Modes {0} - Circuit NOT found on Local copy - [Local copy is now updated]".format(
                online_row['id_mode']))
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# GET GARDEN IRRIGATION CIRCUITS ON SPREADSHEET
def update_configuration():
    global wks, scope, credentials, gc
    try:
        logger.debug('   - Garden configuration')
        f_config = wks.worksheet('Config')
        value_list = f_config.row_values(1)
        list_of_lists = f_config.get_all_records()

        col_config = value_list.index('ID Config') + 1
        col_mode = value_list.index('ID Mode') + 1
        col_rain = value_list.index('Rain Sensor') + 1
        col_moisture = value_list.index('Moisture Sensor') + 1
        col_humidity_level = value_list.index('Humidity Level') + 1
        col_cancel = value_list.index('Irrigation Cancel') + 1
        col_cancel_day = value_list.index('Irrigation Cancel Day') + 1
        col_now = value_list.index('Irrigation Now') + 1
        col_default_sequence = value_list.index('Default Irrigation Sequence') + 1
        col_edited = value_list.index('Edited') + 1

        database.configuration_get()

        row_counter = 1
        for online_row in list_of_lists:
            found_config = False
            row_counter += 1
            if str(online_row['ID Garden']) != str(config.GARDEN_ID_Local):
                continue
            for local_row in config.GENERAL_Configuration:
                found_config = True
                dt_remote = datetime.strptime(online_row['Edited'], "%d/%m/%Y %H:%M:%S")
                dt_local = local_row['edited'].replace(microsecond=0)
                if dt_remote == dt_local:
                    logger.debug("Config Garden {0} - Both copies are updated".format(local_row['id_garden']))
                    pass
                elif dt_remote > dt_local:
                    database.configuration_update(online_row)
                    database.configuration_get()
                    logger.info("Config Garden {0} - Online copy is newer - [Local copy is now updated]".format(
                        local_row['id_garden']))
                else:
                    f_config.update_cell(row_counter, col_config, local_row['id_config'])
                    f_config.update_cell(row_counter, col_mode, local_row['id_mode'])
                    f_config.update_cell(row_counter, col_rain, str(local_row['rain_sensor'] == '1').upper())
                    f_config.update_cell(row_counter, col_moisture, str(local_row['moisture_sensor'] == '1').upper())
                    f_config.update_cell(row_counter, col_humidity_level, local_row['humidity_level'])
                    f_config.update_cell(row_counter, col_cancel, str(local_row['irrigation_cancel'] == '1').upper())
                    f_config.update_cell(row_counter, col_cancel_day,
                                         local_row['irrigation_cancel_day'].strftime("%d/%m/%Y"))
                    f_config.update_cell(row_counter, col_now, str(local_row['irrigation_now'] == '1').upper())
                    f_config.update_cell(row_counter, col_default_sequence, local_row['default_irrigation_sequence'])
                    f_config.update_cell(row_counter, col_edited, local_row['edited'].strftime("%d/%m/%Y %H:%M:%S"))
                    logger.info("Config Garden {0} - Local copy is newer - [Online copy is now updated]".format(
                        local_row['id_garden']))
            if not found_config:
                database.configuration_insert(online_row)
                database.configuration_get()
                logger.info("Config Garden {0} - Circuit NOT found on Local copy - [Local copy is now updated]".format(
                    online_row['id_garden']))
    except Exception, msg:
        logger.error("   ? Connecting to on-line spreadsheet (update_configuration): " + str(msg))
        check_connection()
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# SYNC ONLINE (Update with App-sheet)
t_sync = None
t_sync_stop = False
t_sync_now = False


def sync_online():
    global t_sync
    try:
        logger.debug('   - Sync Online Thread Start:')
        t_sync = threading.Thread(target=sync_online_thread)
        t_sync.start()
    except Exception, msg:
        logger.error("   ? ERROR: Thread to Sync Online: " + str(msg))
        return False


def sync_online_thread():
    global t_sync_stop, t_sync_now, is_connected
    while not t_sync_stop:
        try:
            fact = datetime.now()
            # CHECK CONFIGURATION UPDATE -- Every 30 seconds
            if fact.second % 30 == 0 and fact.microsecond < 150000:
                if internet_on():
                    try:
                        update_configuration()
                        database.get_config()
                        # CHECK MANUAL MODE
                        # if io.check_irrigation_now():
                        #     update_sequences()
                        #     database.get_sequences()
                        #     io.irrigation_now_flag()
                        #     sync_online(False)
                    except Exception, msg:
                        logger.error("  ERROR with SmartApp connection by: {0}".format(msg))
            # SYNC On-line Database with Local Database -- Every 5 minutes with 20 seconds
            if (fact.minute % 5 == 0 and fact.second == 20 and fact.microsecond < 150000) or t_sync_now:
                t_sync_now = False
                if internet_on():
                    if fact.minute == 35:
                        is_connected = connect_spreadsheet()
                        update_forecast()
                    else:
                        update_app_sensors()
                    if check_connection():
                        logger.debug("  Update information on Spreadsheet")
                        update_gardens_information()
                        update_irrigation_circuits()
                        update_sequences()
                        update_automatic_days()
                        update_modes()
                        update_configuration()
                        # -----------------------------------------------
                        database.get_garden_information()
                        database.get_config()
                        database.get_irrigation_circuits()
                        database.get_sequences()
                        database.get_irrigation_days()
                    else:
                        logger.error("   Can't connect with SmartApp (check internet connection)")
        except Exception, msg:
            logger.error("  ERROR with SmartApp connection by: {0}".format(msg))
    logger.info("Thread SmartApp Sync End")
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# UPDATE SENSORS (Humidity and Rain)
def update_app_sensors():
    try:
        logger.debug('   - Gardens sensors:')
        t_sensors = threading.Thread(target=thread_update_sensors_values)
        t_sensors.start()
    except Exception, msg:
        logger.error("   ? ERROR: Thread to Sensor Status: " + str(msg))
        return False


def thread_update_sensors_values():
    try:
        a = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(12))
        logger.debug('   - Gardens sensors (Thread): Send POST to app')
        url = "https://api.appsheet.com/api/v1/apps/6a9a6990-6776-4442-a9b1-977e250a3e96/tables/Weather"
        headers = {
            'ApplicationAccessKey': "itHUQ-k7Nnl-Uctqz-pXgHa-70xs9-K2h0w-2swRh-lZ13P",
            'Content-Type': "application/json",
            'cache-control': "no-cache",
            'Postman-Token': "d11898fa-beb0-41d4-b22c-" + a
        }
        payload = ("{"
                   "\"Action\": \"Edit\","
                   "\"Properties\": {"
                   "\"Locale\": \"en-US\","
                   "\"Timezone\": \"Central America Standard Time\""
                   "},"
                   "\"Rows\": ["
                   "{"
                   "\"ID Garden\": \"" + str(config.GARDEN_ID_Local) + "\","
                   "\"Rain Sensor\": \"" + str(config.SENSOR_Rain_Today).upper() + "\","
                   "\"Moisture Sensor 1\": \"" + str(config.SensorADC_Decode[0]) + "\","
                   "\"Moisture Sensor 2\": \"" + str(config.SensorADC_Decode[1]) + "\","
                   "\"Moisture Sensor 3\": \"" + str(config.SensorADC_Decode[2]) + "\","
                   "\"Moisture Sensor 4\": \"" + str(config.SensorADC_Decode[3]) + "\","
                   "\"Time\": \"" + datetime.now().strftime("%m/%d/%Y %H:%M:00") + "\""
                   "}"
                   "]"
                   "}"
                   "")
        response = requests.request("POST", url, data=payload, headers=headers)
        logger.debug('   - Gardens sensors (Thread): Success: {0}'.format(response.json().get('Success')))
        if not response.json().get('Success'):
            logger.debug('   - Gardens sensors (Thread) Response: {0}'.format(response.text))
    except Exception, msg:
        logger.error("   ? POST_Weather Error: " + str(msg))
        pass
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# UPDATE WEATHER (FORECAST)
def update_forecast():
    try:
        logger.debug('   - Gardens forecast:')
        t_forecast = threading.Thread(target=thread_update_weather)
        t_forecast.start()
    except Exception, msg:
        logger.error("   ? ERROR: Thread to Forecast: " + str(msg))
        return False


def thread_update_weather():
    try:
        a = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(12))
        logger.debug('   - Gardens forecast (Thread): Get forecast')
        w = forecast.get_forecast()
        if w is None:
            return

        logger.debug('   - Gardens forecast (Thread): Send POST to app')
        url = "https://api.appsheet.com/api/v1/apps/6a9a6990-6776-4442-a9b1-977e250a3e96/tables/Weather"
        payload = ("{" 
                   "\"Action\": \"Add\","
                   "\"Properties\": {"
                   "\"Locale\": \"en-US\","
                   "\"Timezone\": \"Central America Standard Time\""
                   "},"
                   "\"Rows\": ["
                   "{"
                   "\"ID Garden\": \"" + str(config.GARDEN_ID_Local) + "\","
                   "\"Rain Sensor\": \"" + str(config.SENSOR_Rain_Today).upper() + "\","
                   "\"Moisture Sensor 1\": \"" + str(config.SensorADC_Decode[0]) + "\","
                   "\"Moisture Sensor 2\": \"" + str(config.SensorADC_Decode[1]) + "\","
                   "\"Moisture Sensor 3\": \"" + str(config.SensorADC_Decode[2]) + "\","
                   "\"Moisture Sensor 4\": \"" + str(config.SensorADC_Decode[3]) + "\","
                   "\"Summary\": \"" + w['summary'] + "\","
                   "\"Icon\": \"" + w['icon'] + "\","
                   "\"Time\": \"" + "\","
                   "\"Temperature\": \"" + str(w['temperature']) + "\","
                   "\"Temperature Min\": \"" + str(w['temperatureMin']) + "\","
                   "\"Temperature Max\": \"" + str(w['temperatureMax']) + "\","
                   "\"Apparent Temperature\": \"" + str(w['apparentTemperature']) + "\","
                   "\"Humidity\": \"" + str(w['humidity']) + "\","
                   "\"Sunrise Time\": \"" + str(w['sunriseTime'].strftime("%m/%d/%Y %H:%M:%S")) + "\","
                   "\"Sunset Time\": \"" + str(w['sunsetTime'].strftime("%m/%d/%Y %H:%M:%S")) + "\","
                   "\"Moon Phase\": \"" + str(w['moonPhase']) + "\","
                   "\"Visibility\": \"" + str(w['visibility']) + "\","
                   "\"Cloud Cover\": \"" + str(w['cloudCover']) + "\","
                   "\"Pressure\": \"" + str(w['pressure']) + "\","
                   "\"Ozone\": \"" + str(w['ozone']) + "\","
                   "\"Wind Speed\": \"" + str(w['windSpeed']) + "\","
                   "\"Wind Gust\": \"" + str(w['windGust']) + "\","
                   "\"Wind Bearing\": \"" + str(w['windBearing']) + "\","
                   "\"UV Index\": \"" + str(w['uvIndex']) + "\","
                   "\"Precip Probability\": \"" + str(w['precipProbability']) + "\","
                   "\"Precip Type\": \"" + str(w['precipType']) + "\","
                   "\"Precip Intensity\": \"" + str(w['precipIntensity']) + "\","
                   "\"Precip Intensity Max\": \"" + str(w['precipIntensityMax']) + "\""
                   "}"
                   "]"
                   "}"
                   "")
        headers = {
            'ApplicationAccessKey': "itHUQ-k7Nnl-Uctqz-pXgHa-70xs9-K2h0w-2swRh-lZ13P",
            'Content-Type': "application/json",
            'cache-control': "no-cache",
            'Postman-Token': "b0349552-63af-4aa2-ad52-" + a
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        logger.debug('   - Gardens forecast (Thread): End POST to app')
        logger.debug('   - Gardens forecast (Thread): Success: {0}'.format(response.json().get('Success')))
    except Exception, msg:
        logger.error("   ? POST_Weather Error: " + str(msg))
        pass
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# UPDATE LOG EVENTS
def log_events(id_event, description, value_log):
    try:
        logger.debug('   - Log Events: [{0} - {1} - [2]]'.format(id_event, description, value_log))
        t_log = threading.Thread(target=thread_update_log_events, args=(id_event, description, value_log))
        t_log.start()
    except Exception, msg:
        logger.error("   ? ERROR: Thread to Log Sensor: " + str(msg))
        return False


def thread_update_log_events(id_event, description, value_log):
    # save data on DataBase
    try:
        database.log_event(str(id_event), description, str(value_log))
    except Exception, msg:
        logger.error("   ? DB LogEvent Error: " + str(msg))
    # save data on Spreadsheet
    try:
        a = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(12))
        url = "https://api.appsheet.com/api/v1/apps/6a9a6990-6776-4442-a9b1-977e250a3e96/tables/Log_Events"
        payload = ("{" 
                   "\"Action\": \"Add\","
                   "\"Properties\": {"
                   "\"Locale\": \"en-US\","
                   "\"Timezone\": \"Central America Standard Time\""
                   "},"
                   "\"Rows\": ["
                   "{"
                   "\"ID Garden\": \"" + str(config.GARDEN_Id) + "\","
                   "\"ID Event\": \"" + str(id_event) + "\", "
                   "\"Description\": \"" + str(description) + "\","
                   "\"Value Log\": \"" + str(value_log) + "\","
                   "\"email\": \"" + str(config.GARDEN_User) + "\""
                   "}"
                   "]"
                   "}"
                   "")
        headers = {
            'ApplicationAccessKey': "itHUQ-k7Nnl-Uctqz-pXgHa-70xs9-K2h0w-2swRh-lZ13P",
            'Content-Type': "application/json",
            'cache-control': "no-cache",
            'Postman-Token': "d1df78be-e60f-45f8-8e22-" + a
        }
        response = requests.request("POST", url, data=payload, headers=headers)
        logger.debug("Log Events Response: [Success: {0}]".format(response.json().get('Success')))
        if not response.json().get('Success'):
            logger.debut('   - Log Events Payload:  {0}'.format(payload))
            logger.debug('   - Log Events Response: {0}'.format(response.text))
    except Exception, msg:
        logger.error("   ? POST Error: " + str(msg))
        pass
# ----------------------------------------------------------------------------------------------------------------------
    # response.text =
    # {"HasWarning":False,
    # "ReturnedFromCache":False,
    # "DisconnectDetected":False,
    # "RowValues":'null',
    # "Success":True,
    # "ErrorDescription":'null',
    # "Timestamp":"2019-01-21T22:25:11.1003547Z",
    # "BackendVersion":5.1,"RequiredIOSAppVersion":4.3,"RequiredAndroidAppVersion":3.0}
# ----------------------------------------------------------------------------------------------------------------------
