import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008
import Adafruit_ADS1x15
import logging
import time
import config

logger = logging.getLogger('sensors.ADC')

# Create an ADS1115 ADC (16-bit) instance.   Or create an ADS1015 ADC (12-bit) instance.
adc = Adafruit_ADS1x15.ADS1115()
# adc = Adafruit_ADS1x15.ADS1015()
# Note you can change the I2C address from its default (0x48), and/or the I2C
# bus by passing in these optional parameters:
# adc = Adafruit_ADS1x15.ADS1015(address=0x49, busnum=1)

GAIN = 1
# Software SPI configuration:
# CLK = 18
# MISO = 23
# MOSI = 24
# CS = 25
# mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

logger.debug('-' * 57)
logger.debug("Start ADC sensor reading")
logger.debug('-' * 57)
config.SensorADC = [0] * 4
config.SensorADC_Decode = [0] * 4
Max_Value = 32700.00
Min_Value = 12000.00
Is_Reverse = True


def read_adc():
    value_sensors = '| '
    value_sensors_decode = '| '
    for i in range(len(config.SensorADC)):
        try:
            config.SensorADC[i] = 0
            # sensorADC[i] = mcp.read_adc(i)
            config.SensorADC[i] = adc.read_adc(i, gain=GAIN)
            temp = ((config.SensorADC[i] - Min_Value) * 100) / (Max_Value - Min_Value)
            if Is_Reverse:
                config.SensorADC_Decode[i] = 100 - temp
            else:
                config.SensorADC_Decode[i] = temp
            if config.SensorADC_Decode[i] < 0:
                config.SensorADC_Decode[i] = 0
            if config.SensorADC_Decode[i] > 100:
                config.SensorADC_Decode[i] = 100
            value_sensors += '{0:>7} | '.format(config.SensorADC[i])
            value_sensors_decode += '{0:06.2f}% | '.format(config.SensorADC_Decode[i])
            time.sleep(0.05)
        except Exception:
            logger.error('get ADC Sensors', exc_info=True)

    logger.debug(value_sensors)
    logger.debug(value_sensors_decode)

