import smbus
import time
import config


# Define some device parameters
I2C_ADDRESS = config.LCD_ADD  # 0x3F  # I2C device address

# Define some device constants
LCD_CHR = 1  # Mode - Sending data
LCD_CMD = 0  # Mode - Sending command

LCD_WIDTH = 16  # Maximum characters per line
LCD_LINE_1 = 0x80  # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0  # LCD RAM address for the 2nd line
LCD_LINE_3 = 0x94  # LCD RAM address for the 3rd line
LCD_LINE_4 = 0xD4  # LCD RAM address for the 4th line

LCD_BACK_LIGHT = 0x08  # On
# LCD_BACK_LIGHT = 0x00  # Off

ENABLE = 0b00000100  # Enable bit

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005

# Open I2C interface
bus = smbus.SMBus(1)  # Rev 2 Pi uses 1


def lcd_init():
    # Initialise display
    lcd_byte(0x33, LCD_CMD)  # 110011 Initialise
    lcd_byte(0x32, LCD_CMD)  # 110010 Initialise
    lcd_byte(0x06, LCD_CMD)  # 000110 Cursor move direction
    lcd_byte(0x0C, LCD_CMD)  # 001100 Display On,Cursor Off, Blink Off
    lcd_byte(0x28, LCD_CMD)  # 101000 Data length, number of lines, font size
    lcd_byte(0x01, LCD_CMD)  # 000001 Clear display
    time.sleep(E_DELAY)


def lcd_backlight_on():
    global LCD_BACK_LIGHT
    LCD_BACK_LIGHT = 0x08  # On
    lcd_byte(0x01, LCD_CMD)


def lcd_backlight_off():
    global LCD_BACK_LIGHT
    LCD_BACK_LIGHT = 0x00  # Off
    lcd_byte(0x01, LCD_CMD)


def lcd_byte(bits, mode):
    # Send byte to data pins
    # bits = the data
    # mode = 1 for data
    #        0 for command
    bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT
    bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT
    # High bits
    bus.write_byte(I2C_ADDRESS, bits_high)
    lcd_toggle_enable(bits_high)
    # Low bits
    bus.write_byte(I2C_ADDRESS, bits_low)
    lcd_toggle_enable(bits_low)


def lcd_toggle_enable(bits):
    # Toggle enable
    time.sleep(E_DELAY)
    bus.write_byte(I2C_ADDRESS, (bits | ENABLE))
    time.sleep(E_PULSE)
    bus.write_byte(I2C_ADDRESS, (bits & ~ENABLE))
    time.sleep(E_DELAY)


def initialize():
    lcd_init()


def lcd_string(message, line):
    # Send string to display
    # message = message.ljust(LCD_WIDTH, " ")
    if line == 1:
        line2 = LCD_LINE_1
    elif line == 2:
        line2 = LCD_LINE_2
    elif line == 3:
        line2 = LCD_LINE_3
    elif line == 4:
        line2 = LCD_LINE_4
    else:
        line2 = LCD_LINE_1

    lcd_byte(line2, LCD_CMD)
    for i in range(LCD_WIDTH):
        lcd_byte(ord(message[i]), LCD_CHR)


def lcd_string_centered(message, line):
    message = message.center(LCD_WIDTH, " ")
    lcd_string(message, line)


def lcd_string_left(message, line):
    message = message.ljust(LCD_WIDTH, " ")
    lcd_string(message, line)


def lcd_string_right(message, line):
    message = message.rjust(LCD_WIDTH, " ")
    lcd_string(message, line)


def lcd_char(message, line, pos):
    # Send string to display
    if line == 1:
        line = LCD_LINE_1
    elif line == 2:
        line = LCD_LINE_2
    elif line == 3:
        line = LCD_LINE_3
    elif line == 4:
        line = LCD_LINE_4
    else:
        line = LCD_LINE_1

    if pos < 0:
        pos += LCD_WIDTH + 1

    lcd_byte(line + pos, LCD_CMD)
    for i in range(len(message)):
        lcd_byte(ord(message[i]), LCD_CHR)


def lcd_clean_line(line):
    message = '                '.ljust(LCD_WIDTH, " ")
    lcd_string(message, line)


def lcd_clean_all():
    lcd_clean_line(1)
    lcd_clean_line(2)
    lcd_clean_line(3)
    lcd_clean_line(4)


def main():
    while True:
        # Send some test
        lcd_string("RPiSpy         <", LCD_LINE_1)
        lcd_string("I2C LCD        <", LCD_LINE_2)

        time.sleep(3)

        # Send some more text
        lcd_string(">         RPiSpy", LCD_LINE_1)
        lcd_string(">        I2C LCD", LCD_LINE_2)

        time.sleep(3)


