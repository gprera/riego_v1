import datetime
from time import sleep
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

relay_1 = 6
relay_2 = 12
relay_3 = 13
relay_4 = 19
relay_5 = 16
relay_6 = 26
relay_7 = 20
relay_8 = 21

section_a = 6
section_b = 19
section_c = 5
section_d = 13

GPIO.setup(section_a, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_b, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_c, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_d, GPIO.OUT, initial=GPIO.HIGH)


def on_sprinkler(section_sprinkler, sector):
    GPIO.output(section_sprinkler, GPIO.LOW)
    print "   - Section " + str(sector) + " >> ON"


def off_sprinkler(section_sprinkler, sector):
    GPIO.output(section_sprinkler, GPIO.HIGH)
    print "   - Section " + str(sector) + " >> OFF"


def sequence():
    on_sprinkler(section_a, 'A')
    sleep(4 * 60)
    off_sprinkler(section_a, 'A')
    # SECTION B
    on_sprinkler(section_b, 'B')
    sleep(4 * 60)
    off_sprinkler(section_b, 'B')
    # SECTION C
    on_sprinkler(section_c, 'C')
    sleep(4 * 60)
    off_sprinkler(section_c, 'C')
    # SECTION A-B
    on_sprinkler(section_a, 'A')
    on_sprinkler(section_b, 'B')
    sleep(4 * 60)
    off_sprinkler(section_a, 'A')
    off_sprinkler(section_b, 'B')
    # SECTION D
    on_sprinkler(section_d, 'D')
    sleep(4 * 60)
    off_sprinkler(section_d, 'D')
    # SECTION C-D
    on_sprinkler(section_c, 'C')
    on_sprinkler(section_d, 'D')
    sleep(4 * 60)
    off_sprinkler(section_c, 'C')
    off_sprinkler(section_d, 'D')


def sequence_a():
    # SECTION A
    on_sprinkler(section_a, 'A')
    sleep(4 * 60)
    off_sprinkler(section_a, 'A')
    # SECTION B
    on_sprinkler(section_b, 'B')
    sleep(4 * 60)
    off_sprinkler(section_b, 'B')
    # SECTION C
    on_sprinkler(section_c, 'C')
    sleep(4 * 60)
    off_sprinkler(section_c, 'C')
    # SECTION A-B
    on_sprinkler(section_a, 'A')
    on_sprinkler(section_b, 'B')
    sleep(4 * 60)
    off_sprinkler(section_a, 'A')
    off_sprinkler(section_b, 'B')
    # SECTION C
    on_sprinkler(section_c, 'C')
    sleep(4 * 60)
    off_sprinkler(section_c, 'C')


def sequence_b():
    # SECTION D
    on_sprinkler(section_d, 'D')
    sleep(4 * 60)
    off_sprinkler(section_d, 'D')
    sleep(1 * 60)
    # SECTION C-D
    on_sprinkler(section_d, 'D')
    sleep(4 * 60)
    off_sprinkler(section_d, 'D')


while True:
    sprinkler_today = datetime.datetime.now()
    # sequence_a()
    if sprinkler_today.weekday() == 0 or sprinkler_today.weekday() == 2 or sprinkler_today.weekday() == 4:
        if sprinkler_today.hour == 22 and sprinkler_today.minute == 0:
            print "************************"
            print str(sprinkler_today)
            print "    Init irrigation    "
            print "       JARDIN 1        "
            print "> Sequence 1th time: "
            sequence_a()
            sleep(1 * 60)
            print "> Sequence 2nd time: "
            sequence_a()
            print "************************"
            sprinkler_today = datetime.datetime.now()
            print str(sprinkler_today)
            print "    END irrigation    "
            print "************************"
    if sprinkler_today.weekday() == 1 or sprinkler_today.weekday() == 3 or sprinkler_today.weekday() == 5:
        if sprinkler_today.hour == 22 and sprinkler_today.minute == 0:
            print "************************"
            print str(sprinkler_today)
            print "    Init irrigation    "
            print "       JARDIN 2        "
            print "> Sequence 1th time: "
            sequence_b()
            sleep(1 * 60)
            print "> Sequence 2nd time: "
            sequence_b()
            print "************************"
            sprinkler_today = datetime.datetime.now()
            print str(sprinkler_today)
            print "    END irrigation    "
            print "************************"
    if sprinkler_today.weekday() == 6:
        if sprinkler_today.hour == 22 and sprinkler_today.minute == 0:
            print "************************"
            print str(sprinkler_today)
            print "    Init irrigation    "
            print "       JARDIN 1        "
            print "> Sequence 1th time: "
            sequence_a()
            sleep(1 * 60)
            print "> Sequence 2nd time: "
            sequence_a()
            print "************************"
            sprinkler_today = datetime.datetime.now()
            print str(sprinkler_today)
            print "************************"

            print str(sprinkler_today)
            print "    Init irrigation    "
            print "       JARDIN 2        "
            print "> Sequence 1th time: "
            sequence_b()
            sleep(1 * 60)
            print "> Sequence 2nd time: "
            sequence_b()
            print "************************"
            sprinkler_today = datetime.datetime.now()
            print str(sprinkler_today)
            print "    END irrigation    "
            print "************************"

    sleep(30)
            # 9pm
            # SECTION A
            # print "> Sequence 1: "
            # sequence()
            # sleep(1 * 60)
            # print "> Sequence 2: "
            # sequence()
            # print "************************"
            # print "    END irrigation    "
            # print "************************"

