import datetime
import logging
from time import sleep
import RPi.GPIO as GPIO
import config
import database

logger = logging.getLogger('sensors')
logger2 = logging.getLogger('main.sensors')

GPIO.setmode(GPIO.BCM)

# ----------------------------------------------------
section_1 = 6
section_2 = 12
section_3 = 13
section_4 = 19
section_5 = 16
section_6 = 26
section_7 = 20
section_8 = 21
# ----------------------------------------------------
GPIO.setup(section_1, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_2, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_3, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_4, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_5, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_6, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_7, GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(section_8, GPIO.OUT, initial=GPIO.HIGH)
# ----------------------------------------------------
rain_1 = 18
rain_2 = 23
rain_3 = 24
GPIO.setup(rain_1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(rain_2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(rain_3, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
# ----------------------------------------------------
button_a = 17
button_b = 27
button_c = 22
GPIO.setup(button_a, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(button_b, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(button_c, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
# ----------------------------------------------------


button_a_oldstatus = 0
button_b_oldstatus = 0
button_a_status = 0
button_b_status = 0

rain_control = 0
rain_today = False


def check_inputs():
    global button_a_status, button_b_status
    button_a_status = 0
    button_b_status = 0
    if GPIO.input(button_a) == 1:
        logger.debug('Button_A pressed')
        button_a_status = 1
        sleep(0.1)
    if GPIO.input(button_b) == 1:
        logger.debug('Button_B pressed')
        button_b_status = 1
        sleep(0.1)
    # if button_a_status == 1:
    #    sleep(1)
    #    if GPIO.input(button_a) == 1:
    #        logger.debug('Button_A pressed delayed')
    #        button_a_status = 0
    #        button_b_status = 1
    #        sleep(0.1)


def wait_button1_press():
    global button_a_status, button_b_status
    button_a_status = 0
    button_b_status = 0
    for x in range(0, 1000):
        if GPIO.input(button_a) == 1:
            logger.debug('Button_A: pressed normal')
            button_a_status = 1
            sleep(0.5)
            break
        if GPIO.input(button_b) == 1:
            logger.debug('Button_B: pressed normal')
            button_b_status = 1
            sleep(0.5)
            break
        sleep(0.01)


def check_inputs_values():
    global button_a_status, button_b_status
    for x in range(0, 30):
        print('S1: {0}   S2: {1}  S3: {2}'.format(GPIO.input(button_a), GPIO.input(button_b), GPIO.input(button_c)))
        sleep(0.5)


def check_rain_sensors():
    global rain_control, rain_today
    logger.debug('Rain Sensor: [a:{0}][b:{1}][c:{2}]  >> [Today:{3}]'.format(GPIO.input(rain_1), GPIO.input(rain_2),
                                                                             GPIO.input(rain_3), rain_today))
    if GPIO.input(rain_1) or GPIO.input(rain_2) or GPIO.input(rain_3):
        if rain_control <= 20:
            rain_control += 1
        if rain_control > 10 and rain_control != 100:
            rain_today = True
            rain_control = 100
            logger2.info('Rain Start')
            database.save_event(config.GARDEN_ID, datetime.datetime.now(), 'Rain Start')
    else:
        if rain_today and rain_control == 100:
            logger2.info('Rain Stop')
            rain_control = 0
            database.save_event(config.GARDEN_ID, datetime.datetime.now(), 'Rain Stop')


def clear_rain_sensor():
    global rain_control, rain_today
    rain_control = 0
    if rain_today:
        logger2.info('Rain Stop by time')
        rain_today = False
        database.save_event(config.GARDEN_ID, datetime.datetime.now(), 'Rain Stop by time')


def check_mode_state():

    if config.GENERAL_ModoActual == 1:  # MODE: AUTOMATIC OFF
        logger.info("*****  Automatic Mode OFF  *****  ")
        return
    if config.GENERAL_Estado == 1:      # STATE: COMPLETE OFF
        logger.info("*****  General State - ALL OFF  *****  ")
        return
    # -------------------------------------------------------------------------
    if config.GENERAL_Estado == 0:      # STATE: NORMAL ON
        pass
    if config.GENERAL_ModoActual == 2:  # MODE: AUTOMATIC WITH SENSORS
        logger.info("*****  Automatic Mode: SENSORS  *****  ")
        check_automatic_sensors()
        return
    if config.GENERAL_ModoActual == 3:  # MODE: AUTOMATIC WITH SCHEDULE
        logger.info("*****  Automatic Mode: SCHEDULE  *****  ")
        check_automatic_schedule()
        return


def on_sprinkler(section_sprinkler, sector):
    if section_sprinkler is None:
        return
    GPIO.output(section_sprinkler, GPIO.LOW)
    logger.debug("   - Section " + str(sector) + " >> ON")


def off_sprinkler(section_sprinkler, sector):
    GPIO.output(section_sprinkler, GPIO.HIGH)
    logger.debug("   - Section " + str(sector) + " >> OFF")


def individual_sequence(sector):
    if sector == 0:
        return None
    if sector == 1:
        return section_1
    elif sector == 2:
        return section_2
    elif sector == 3:
        return section_3
    elif sector == 4:
        return section_4
    elif sector == 5:
        return section_5
    elif sector == 6:
        return section_6
    elif sector == 7:
        return section_7
    elif sector == 8:
        return section_8
    else:
        return None


def sequence_db():
    for _row in config.IRRIGATION_Sequence:
        logger.debug('Row: {0}'.format(_row))
        for i in range(1, 10):
            str_sequence = 'seq' + str(i)
            logger.debug('   Sequence: {0}'.format(str_sequence))
            if _row[str_sequence] is None:
                continue
            for _seq in _row[str_sequence]:
                on_sprinkler(individual_sequence(_seq), _seq)
            sleep(config.GENERAL_TiempoRiego)
            for _seq in _row[str_sequence]:
                off_sprinkler(individual_sequence(_seq), _seq)


def manual_irrigation():
    logger.debug('**** Irrigation by manual instruction  *****')
    sequence_db()


def check_automatic_schedule():
    try:
        sprinkler_today = datetime.datetime.now()
        database.get_sequences()
        for _row in config.IRRIGATION_Sequence:
            if (_row['auto_dias'][sprinkler_today.weekday()] and
               sprinkler_today.strftime('%H:%M') == _row['auto_hora'].strftime('%H:%M')):
                # NEXT IRRIGATION CANCELED
                if config.GENERAL_Estado == 2:  # STATE: NEXT OFF
                    logger.info("*****  General State - NEXT IRRIGATION OFF  *****  ")
                    database.save_state(0)
                    config.GENERAL_Estado = 0
                    break
                # NORMAL MODE
                logger.debug('**** Irrigation by schedule START  *****')
                sequence_db()
    except Exception, msg:
        logger.error("Error: " + str(msg))


def check_automatic_sensors():
    try:
        sprinkler_today = datetime.datetime.now()
        database.get_sequences()
        for _row in config.IRRIGATION_Sequence:

            if (_row['auto_dias'][sprinkler_today.weekday()] and
                        sprinkler_today.strftime('%H:%M') == _row['auto_hora'].strftime('%H:%M')):
                # NEXT IRRIGATION CANCELED
                if config.GENERAL_Estado == 2:  # STATE: NEXT OFF
                    logger.info("*****  General State - NEXT IRRIGATION OFF  *****  ")
                    database.save_state(0)
                    config.GENERAL_Estado = 0
                    break
                # NORMAL MODE
                logger.debug('**** Irrigation by schedule START  *****')
                sequence_db()
    except Exception, msg:
        logger.error("Error: " + str(msg))


def check_automatic_sensors():
    try:
        sprinkler_today = datetime.datetime.now()
        database.get_sequences()
        for _row in config.IRRIGATION_Sequence:
            if (sprinkler_today.strftime('%H:%M') == _row['auto_hora'].strftime('%H:%M') and
                    config.SensorADC_Decode[0] < config.GENERAL_SensorHumedad and
                    config.SensorADC_Decode[1] < config.GENERAL_SensorHumedad and
                    config.SensorADC_Decode[2] < config.GENERAL_SensorHumedad):
                # NEXT IRRIGATION CANCELED
                if config.GENERAL_Estado == 2:  # STATE: NEXT OFF
                    logger.info("*****  General State - NEXT IRRIGATION OFF  *****  ")
                    database.save_state(0)
                    config.GENERAL_Estado = 0
                    break
                # NORMAL MODE
                logger.debug('**** Irrigation by sensors START  *****')
                sequence_db()
    except Exception, msg:
        logger.error("Error: " + str(msg))
