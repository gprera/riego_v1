import time

LCD_BACKLIGHT = 0x08  # On


def lcd_init():
    pass


def lcd_backlight_on():
    global LCD_BACKLIGHT
    LCD_BACKLIGHT = 0x08  # On


def lcd_backlight_off():
    global LCD_BACKLIGHT
    LCD_BACKLIGHT = 0x00  # Off


def lcd_byte(bits, mode):
    pass


def lcd_toggle_enable(bits):
    pass


def initialize():
    lcd_init()


def lcd_string(message, line):
    pass


def lcd_string_centered(message, line):
    pass


def lcd_string_left(message, line):
    pass


def lcd_string_right(message, line):
    pass


def lcd_char(message, line, pos):
    pass


def lcd_clean_line(line):
    pass


def lcd_clean_all():
    pass


def main():
    pass


