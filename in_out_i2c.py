import time
import datetime
import logging
from time import sleep
import RPi.GPIO as GPIO
import Adafruit_ADS1x15
import config
import smbus
import database
import smart_app

logger = logging.getLogger('sensors')
logger2 = logging.getLogger('main.sensors')
# ----------------------------------------------------
GPIO.setmode(GPIO.BCM)
# ----------------------------------------------------
# OUTPUTS: Digital Outputs
buttons_led = [6, 12, 13, 19, 16, 26, 20, 21]
for bd in buttons_led:
    GPIO.setup(bd, GPIO.OUT, initial=GPIO.HIGH)
output_i2c = [~0x01 & 0xFF, ~0x02 & 0xFF, ~0x04 & 0xFF, ~0x08 & 0xFF,
              ~0x10 & 0xFF, ~0x20 & 0xFF, ~0x40 & 0xFF, ~0x80 & 0xFF]
# ----------------------------------------------------
# INPUTS: Digital Inputs
buttons = [17, 18, 22, 23, 24, 27]
for bs in buttons:
    GPIO.setup(bs, GPIO.IN, pull_up_down=GPIO.PUD_UP)
button_status_old = [False, False, False, False, False, False]
button_status = [False, False, False, False, False, False]
buttons_real = 0
button_change = False
# ----------------------------------------------------
# I2C I/O:
# io_expand_1 = 0x20
io_expand_1 = 0x20
i2c = smbus.SMBus(1)
# ----------------------------------------------------
adc = Adafruit_ADS1x15.ADS1115(address=0x48, busnum=1)
GAIN = 1
config.SensorADC = [0] * 4
config.SensorADC_Decode = [0] * 4
Max_Value = 20350  # 32700.00
Min_Value = 10950  # 12000.00
Disconnect_Value = 6000
Is_Reverse = True

# ----------------------------------------------------
sensor_rain_old = 0
rain_time = 0


# ---------------------------------------------------------------------------------------------------------------------
# Connect to I/O I2C expander board
# ---------------------------------------------------------------------------------------------------------------------
def connect():
    try:
        logger.info('*' * 50)
        logger.info('Connecting with I2C In/Out board')
        logger2.info('Connecting with I2C In/Out board')
        i2c.write_byte(io_expand_1, 0xFF)
        # i2c.write_byte(io_expand_2, 0xFF)
    except Exception, msg:
        logger.error('Error while connecting I2C devices by: {0}'.format(msg))
        logger2.error('Error while connecting I2C devices by: {0}'.format(msg))
        raise
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# Check Inputs: Button panel
# ---------------------------------------------------------------------------------------------------------------------
def check_inputs_panel_buttons():
    global buttons, button_status, button_status_old
    button_status_old = button_status
    for x in range(0, 6):
        if GPIO.input(buttons[x]) == 0:
            logger.debug('INPUT:  Button [{0}]: Pressed'.format(str(x)))
            button_status[x] = True
            sleep(0.1)
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# Wait Inputs: Button panel
# ---------------------------------------------------------------------------------------------------------------------
def wait_buttons_panel_press():
    global buttons, button_status, button_status_old
    button_pressed = False
    for y in range(0, 1000):
        for x in range(0, 6):
            if GPIO.input(buttons[x]) == 0:
                logger.debug('INPUT: Button [{0}]: Pressed normal'.format(str(x)))
                button_status[x] = True
                button_pressed = True
                sleep(0.5)
                break
        if button_pressed:
            break
        sleep(0.01)
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# INPUT: I2C Rain Sensor [7]
# ---------------------------------------------------------------------------------------------------------------------
def check_rain_sensor():
    global sensor_rain_old, rain_time
    try:
        if not config.GARDEN_Config['rain_sensor'] == '1':
            logger.debug('Do not have Rain Sensor')
            return

        sensor_value = bitfield(i2c.read_byte(io_expand_1))
        sensor_rain_old = config.SENSOR_Rain
        logger.debug('INPUT: Rain Sensor Value: {0}-{1}'.format(sensor_value[4] == 1, sensor_value[4]))
        if sensor_value[4] == 0:
            if not config.SENSOR_Rain:
                config.SENSOR_Rain = True
                smart_app.log_events(71, 'Rain Sensor ON: Start rain', '')
                logger.debug('Start Rain sensor')
                logger2.debug('Start Rain Sensor')
            if not config.SENSOR_Rain_Today:
                rain_time += 1
                if rain_time >= 10:
                    config.SENSOR_Rain_Today = True
                    smart_app.log_events(73, 'Rain Sensor BLOCK Irrigation for Today', '')
                    logger.debug('Irrigation cancelled by Rain sensor')
                    logger2.debug('Irrigation cancelled by Rain Sensor')
                    rain_time = 0
            sleep(0.1)
        else:
            if config.SENSOR_Rain:
                config.SENSOR_Rain = False
                smart_app.log_events(72, 'Rain Sensor OFF: Stop rain', '')
                logger.debug('Stop Rain sensor')
                logger2.debug('Stop Rain Sensor')
    except Exception, msg:
        logger.error('ERROR on Check rain sensor: {0}'.format(msg))
        return False
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# INPUT: Clear rain sensor block flag
# ---------------------------------------------------------------------------------------------------------------------
def clear_rain_sensor():
    global rain_time
    rain_time = 0
    if config.SENSOR_Rain_Today:
        logger2.info('Rain Stop by time')
        smart_app.log_events(74, 'Rain Sensor OFF: Stop by time 5:00:00 am', '')
    config.SENSOR_Rain_Today = False
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# Check ADC: check ADC Sensors
# ---------------------------------------------------------------------------------------------------------------------
def check_adc_sensors():
    if not config.GARDEN_Config['moisture_sensor']:
        logger.debug("System do not have sensor for Humidity level")
        return

    value_sensors = '| '
    for i in range(len(config.SensorADC)):
        try:
            config.SensorADC[i] = 0
            config.SensorADC[i] = adc.read_adc(i, gain=GAIN)

            temp = ((config.SensorADC[i] - Min_Value) * 100) / (Max_Value - Min_Value)
            if Is_Reverse:
                if config.SensorADC[i] < Disconnect_Value:
                    config.SensorADC_Decode[i] = 0
                else:
                    config.SensorADC_Decode[i] = 100 - temp
                # if config.SensorADC[i] > Max_Value:
                #     config.SensorADC_Decode[i] = 100
                # else:
                #     config.SensorADC_Decode[i] = 100 - temp
            else:
                config.SensorADC_Decode[i] = temp

            if config.SensorADC_Decode[i] < 0:
                config.SensorADC_Decode[i] = 0
            if config.SensorADC_Decode[i] > 100:
                config.SensorADC_Decode[i] = 100

            value_sensors += '{0:>6} [{1:>4.0f} % ] | '.format(config.SensorADC[i], config.SensorADC_Decode[i])
            time.sleep(0.05)
        except Exception:
            logger.error('get ADC Sensors', exc_info=True)
    logger.debug(value_sensors)


# ---------------------------------------------------------------------------------------------------------------------
# OUTPUT: I2C Valves [0 - 6]
def output_valves_i2c(out_valve, turn_on):
    global buttons_led
    try:
        out_valve = out_valve - 1
        temp_values = i2c.read_byte(io_expand_1)
        old_value_log = temp_values
        old_value_log2 = output_i2c[out_valve]

        if turn_on:
            temp_values = temp_values & output_i2c[out_valve]
            database.log_event(76, 'Output valve ON: ', '{0}'.format(out_valve))
        else:
            temp_values = temp_values | (~ output_i2c[out_valve] & 0xFF)
            database.log_event(77, 'Output valve OFF: ', '{0}'.format(out_valve))

        logger.debug('OUT > Old value {0:b} ... {1:b}  -  Actual value: {2:b}'
                     .format(old_value_log, old_value_log2, temp_values))
        logger2.debug('OUT > Old value {0:b} ... {1:b}  -  Actual value: {2:b}'
                      .format(old_value_log, old_value_log2, temp_values))
        i2c.write_byte(io_expand_1, temp_values)
    except Exception, msg:
        database.log_event(75, 'ERROR in Output valve', '{0}'.format(msg))
        logger.debug('Error output values: {0}'.format(msg))
        logger2.debug('Error output values: {0}'.format(msg))
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# BITFIELD: Convert decimal to binary in correct position
def bitfield(n):
    l_temp = [int(digit) for digit in bin(n)[2:]]
    l_temp = [0] * (8 - len(l_temp)) + l_temp
    return l_temp[::-1]
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# IRRIGATION NOW: Check if the irrigation flag is ON
# ---------------------------------------------------------------------------------------------------------------------
def check_irrigation_now():
    return config.GARDEN_Config['irrigation_now'] == '1'
# ---------------------------------------------------------------------------------------------------------------------


def irrigation_now_flag():
    logger.debug('Irrigation Now Flag is ON')
    logger2.debug('Irrigation Now Flag is ON')
    if config.GARDEN_Config['irrigation_now'] == '1':
        database.manual_irrigation_off()
        database.manual_irrigation_off_sequence()
        if config.GARDEN_Config['id_mode'] == 4:
            logger.info('Irrigation Now: System is locked')
            logger2.info('Irrigation Now: System is locked')
            smart_app.log_events(55, 'Irrigation Now: Stopped because system is locked.', '')
        else:
            for local_row in config.GARDEN_Irrigation_Sequences:
                if local_row['irrigation_now'] == '1':
                    logger.info('Irrigation Now: Start [sequence: {0}]'.format(local_row['id_sequence']))
                    logger2.info('Irrigation Now: Start [sequence: {0}]'.format(local_row['id_sequence']))
                    smart_app.log_events(54, 'Irrigation Now: Start', local_row['id_sequence'])
                    irrigation_start(local_row['id_sequence'])


# ---------------------------------------------------------------------------------------------------------------------
# CHECK MODES:
# ---------------------------------------------------------------------------------------------------------------------
def check_mode():
    if config.GARDEN_Config['id_mode'] == 1:  # Mode: TURN OFF
        pass
    if config.GARDEN_Config['id_mode'] == 2:  # Mode: AUTOMATIC BY MOISTURE SENSORS
        check_moisture_sensor()
    if config.GARDEN_Config['id_mode'] == 3:  # Mode: AUTOMATIC BY SCHEDULE
        check_automatic_schedule()
    if config.GARDEN_Config['id_mode'] == 4:  # Mode: LOCKED
        pass
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# MODE: Moisture Sensor - check schedule and sensors
# ---------------------------------------------------------------------------------------------------------------------
def check_moisture_sensor():
    for local_row in config.GARDEN_Irrigation_Days:
        day_name = datetime.datetime.now().strftime('%A').lower()
        if local_row[day_name] is None:
            # Today is NOT an Irrigation Day
            pass
        else:
            # Today is a Irrigation Day
            if datetime.datetime.now().strftime('%H:%M') == local_row[day_name].strftime("%H:%M"):
                if config.GARDEN_Config['irrigation_cancel'] == '1':  # Irrigation Cancel
                    if config.GARDEN_Config['irrigation_cancel_day'].date() == datetime.datetime.now().date():
                        smart_app.log_events(58, 'Irrigation by Sensor: Stopped because today was canceled.', '')
                        sleep(30)
                else:
                    # Check sensors
                    check_adc_sensors()
                    humidity_level = config.GARDEN_Config['humidity_level']
                    if (config.SensorADC_Decode[0] < humidity_level and
                       config.SensorADC_Decode[1] < humidity_level and
                       config.SensorADC_Decode[2] < humidity_level):
                        smart_app.log_events(56, 'Irrigation by Sensor: Start', '{0}'.format(local_row['id_sequence']))
                        irrigation_start(local_row['id_sequence'])
                    else:
                        smart_app.log_events(57, 'Irrigation by Sensor: Stopped because sensor values', '')
                        sleep(30)
                    # logger.debug('   * Moisture Sensor: {0}'.format(config.GARDEN_Config['moisture_sensor']))

# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# MODE: Schedule - check only schedule for irrigation
# ---------------------------------------------------------------------------------------------------------------------
def check_automatic_schedule():
    for local_row in config.GARDEN_Irrigation_Days:
        day_name = datetime.datetime.now().strftime('%A').lower()
        if local_row[day_name] is None:
            # Today is NOT an Irrigation Day
            pass
        else:
            # Today is a Irrigation Day
            if datetime.datetime.now().strftime('%H:%M') == local_row[day_name].strftime("%H:%M"):
                logger2.info('Rain Today {0}'.format(config.SENSOR_Rain_Today))
                if config.SENSOR_Rain_Today:
                    logger2.info('Irrigation by Schedule: Stopped because today it rained.')
                    smart_app.log_events(52, 'Irrigation by Schedule: Stopped because today it rained.', '')
                    sleep(60)
                elif config.GARDEN_Config['irrigation_cancel'] == '1':  # Irrigation Cancel
                    if config.GARDEN_Config['irrigation_cancel_day'].date() == datetime.datetime.now().date():
                        smart_app.log_events(53, 'Irrigation by Schedule: Stopped because today was canceled.', '')
                        sleep(60)
                    else:
                        smart_app.log_events(51, 'Irrigation by Schedule: Start', '{0}'.format(local_row['id_sequence']))
                        irrigation_start(local_row['id_sequence'])
                        sleep(60)
                else:
                    smart_app.log_events(51, 'Irrigation by Schedule: Start', '{0}'.format(local_row['id_sequence']))
                    irrigation_start(local_row['id_sequence'])
                    sleep(60)
# ---------------------------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------------------------
# IRRIGATION START - Start sequence by ID.
# ---------------------------------------------------------------------------------------------------------------------
def irrigation_start(sequence_name):
    try:
        for local_row in config.GARDEN_Irrigation_Sequences:
            if local_row['id_sequence'] == sequence_name:
                smart_app.log_events(61, 'Inicio de Riego - Sequencia: ' + local_row['sequence_name'],
                                     local_row['sequence_name'])
                for i in range(1, 15):
                    str_sequence = 'seq_{0:02d}'.format(i)
                    logger.debug('  Sequence: {0}'.format(str_sequence))
                    if local_row[str_sequence] is None:
                        continue
                    for seq in local_row[str_sequence]:
                        logger.debug('  - Valve: {0}'.format(seq))
                        output_valves_i2c(seq, True)
                    time_irrigation = local_row['irrigation_time']
                    sleep((time_irrigation.hour * 3600) + (time_irrigation.minute * 60) + time_irrigation.second)
                    for seq in local_row[str_sequence]:
                        output_valves_i2c(seq, False)
    except Exception as e:
        logger.critical("ERROR: Irrigation Start: " + str(e))
        raise

# ---------------------------------------------------------------------------------------------------------------------
