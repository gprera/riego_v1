import sys
import logging
import psycopg2.extras
import config
import time
from datetime import datetime


logger = logging.getLogger(__name__)


def connect(connection_string):
    """ Connects to local database and creates a dict cursor

    :param connection_string: as name implies
    :var conn_a: SQL command for connecting to database
    :var cursor_a: dict cursor created
    :raise: Error if can't connect to database
    """
    global conn_a, cursor_a
    try:
        logger.debug("   - DB-Connecting to database...")
        conn_a = psycopg2.connect(connection_string)
        cursor_a = conn_a.cursor(cursor_factory=psycopg2.extras.DictCursor)
    except psycopg2.OperationalError:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        logger.error("   - DB-Connecting to database: " + exc_type.__name__ +
                     "-" + str(exc_obj).split("\n")[0] + " @" + str(exc_tb.tb_lineno))
        raise
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        logger.error("   - DB-Connecting to database: " + exc_type.__name__ +
                     "-" + str(exc_obj).split("\n")[0] + " @" + str(exc_tb.tb_lineno))
        raise
    else:
        logger.debug("   - DB-Connect successfully.")


def disconnect():
    try:
        logger.debug("   - DB-Disconnecting database...")
        if conn_a:  conn_a.close()
        return True
    except Exception, msg:
        logger.error("   - DB-Disconnecting database: " + str(msg))
        return False


def get_gardens_info():
    try:
        command = "SELECT * FROM gardens"
        cursor_a.execute(command)
        records = cursor_a.fetchone()
        config.GARDEN_Id = records['id_garden']
        config.GARDEN_Serial = str(records['serial'])
        config.GARDEN_Name = records['garden_name']
        config.GARDEN_LCD = records['lcd']
        config.GARDEN_User = records['user_mail']
        config.GARDEN_Active = records['enabled']
        config.GARDEN_Edited = records['edited']
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get Database Gardens: " + str(msg))
        return False


def update_gardens_info():
    try:
        command = "UPDATE gardens SET " \
                  "serial = %s, garden_name = %s, " \
                  "lcd = %s, user_mail = %s, " \
                  "enabled = %s, edited = %s " \
                  "WHERE id_garden = %s"

        cursor_a.execute(command, [str(config.GARDEN_Serial), config.GARDEN_Name, config.GARDEN_LCD, config.GARDEN_User,
                                   str(config.GARDEN_Active), config.GARDEN_Edited, config.GARDEN_Id])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Update Database Gardens: " + str(msg))
        conn_a.rollback()
        return False


def irrigation_circuits_get():
    try:
        command = "SELECT * FROM irrigation_circuits"
        cursor_a.execute(command)
        records = cursor_a.fetchall()
        config.IRRIGATION_Circuits = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get Database Irrigation Circuits: " + str(msg))
        return False


def irrigation_circuits_update(row_update):
    try:
        command = "UPDATE irrigation_circuits SET " \
                  "circuit_name = %s, " \
                  "sprinkles = %s, " \
                  "edited = %s " \
                  "WHERE id_garden = %s AND id_circuit = %s"
        cursor_a.execute(command, [row_update['Circuit Name'], row_update['Sprinkles'],
                                   datetime.strptime(row_update['Edited'], "%d/%m/%Y %H:%M:%S"),
                                   row_update['ID Garden'], row_update['ID Circuit']])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Update Irrigation Circuits: " + str(msg))
        conn_a.rollback()
        return False


def irrigation_circuits_insert(row_update):
    try:
        command = "INSERT INTO irrigation_circuits " \
                  "(id_circuit, id_garden, circuit_name, sprinkles, edited) " \
                  "VALUES (%s, %s, %s, %s, %s)"
        cursor_a.execute(command, [row_update['ID Circuit'], row_update['ID Garden'],
                                   row_update['Circuit Name'], row_update['Sprinkles'],
                                   datetime.strptime(row_update['Edited'], "%d/%m/%Y %H:%M:%S")])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Insert Irrigation Circuits: " + str(msg))
        return False


def sequences_get():
    try:
        command = "SELECT * FROM sequences"
        cursor_a.execute(command)
        records = cursor_a.fetchall()
        config.IRRIGATION_Sequences = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get Database Irrigation Sequences: " + str(msg))
        return False


def sequences_update(r_update):
    try:
        command = "UPDATE sequences SET " \
                  "sequence_name = %s, " \
                  "irrigation_time = %s, " \
                  "seq_01 = %s, seq_02 = %s, seq_03 = %s, seq_04 = %s, seq_05 = %s, " \
                  "seq_06 = %s, seq_07 = %s, seq_08 = %s, seq_09 = %s, seq_10 = %s," \
                  "seq_11 = %s, seq_12 = %s, seq_13 = %s, seq_14 = %s, seq_15 = %s, " \
                  "edited = %s, irrigation_now = %s " \
                  "WHERE id_garden = %s AND id_sequence = %s"

        cursor_a.execute(command,
                         [r_update['Sequence Name'],
                          datetime.strptime(r_update['Irrigation Time'], "%H:%M:%S"),
                          None if r_update['Seq_01'] == '' else [int(x) for x in str(r_update['Seq_01']).split(',')],
                          None if r_update['Seq_02'] == '' else [int(x) for x in str(r_update['Seq_02']).split(',')],
                          None if r_update['Seq_03'] == '' else [int(x) for x in str(r_update['Seq_03']).split(',')],
                          None if r_update['Seq_04'] == '' else [int(x) for x in str(r_update['Seq_04']).split(',')],
                          None if r_update['Seq_05'] == '' else [int(x) for x in str(r_update['Seq_05']).split(',')],
                          None if r_update['Seq_06'] == '' else [int(x) for x in str(r_update['Seq_06']).split(',')],
                          None if r_update['Seq_07'] == '' else [int(x) for x in str(r_update['Seq_07']).split(',')],
                          None if r_update['Seq_08'] == '' else [int(x) for x in str(r_update['Seq_08']).split(',')],
                          None if r_update['Seq_09'] == '' else [int(x) for x in str(r_update['Seq_09']).split(',')],
                          None if r_update['Seq_10'] == '' else [int(x) for x in str(r_update['Seq_10']).split(',')],
                          None if r_update['Seq_11'] == '' else [int(x) for x in str(r_update['Seq_11']).split(',')],
                          None if r_update['Seq_12'] == '' else [int(x) for x in str(r_update['Seq_12']).split(',')],
                          None if r_update['Seq_13'] == '' else [int(x) for x in str(r_update['Seq_13']).split(',')],
                          None if r_update['Seq_14'] == '' else [int(x) for x in str(r_update['Seq_14']).split(',')],
                          None if r_update['Seq_15'] == '' else [int(x) for x in str(r_update['Seq_15']).split(',')],
                          datetime.strptime(r_update['Edited'], "%d/%m/%Y %H:%M:%S"),
                          str(int(r_update['Now'] == 'TRUE')),
                          r_update['ID Garden'], r_update['ID Sequence']])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Update Irrigation Sequences: " + str(msg))
        conn_a.rollback()
        return False


def sequences_insert(r_update):
    try:
        command = "INSERT INTO sequences " \
                  "(id_sequence, sequence_name, id_garden, irrigation_time, " \
                  "seq_01, seq_02, seq_03, seq_04, seq_05, seq_06, seq_07, seq_08, seq_09, seq_10, " \
                  "seq_11, seq_12, seq_13, seq_14, seq_15, edited, irrigation_now) " \
                  "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor_a.execute(command,
                         [r_update['ID Sequence'], r_update['Sequence Name'], r_update['ID Garden'],
                          datetime.strptime(r_update['Irrigation Time'], "%H:%M:%S"),
                          None if r_update['Seq_01'] == '' else [int(x) for x in str(r_update['Seq_01']).split(',')],
                          None if r_update['Seq_02'] == '' else [int(x) for x in str(r_update['Seq_02']).split(',')],
                          None if r_update['Seq_03'] == '' else [int(x) for x in str(r_update['Seq_03']).split(',')],
                          None if r_update['Seq_04'] == '' else [int(x) for x in str(r_update['Seq_04']).split(',')],
                          None if r_update['Seq_05'] == '' else [int(x) for x in str(r_update['Seq_05']).split(',')],
                          None if r_update['Seq_06'] == '' else [int(x) for x in str(r_update['Seq_06']).split(',')],
                          None if r_update['Seq_07'] == '' else [int(x) for x in str(r_update['Seq_07']).split(',')],
                          None if r_update['Seq_08'] == '' else [int(x) for x in str(r_update['Seq_08']).split(',')],
                          None if r_update['Seq_09'] == '' else [int(x) for x in str(r_update['Seq_09']).split(',')],
                          None if r_update['Seq_10'] == '' else [int(x) for x in str(r_update['Seq_10']).split(',')],
                          None if r_update['Seq_11'] == '' else [int(x) for x in str(r_update['Seq_11']).split(',')],
                          None if r_update['Seq_12'] == '' else [int(x) for x in str(r_update['Seq_12']).split(',')],
                          None if r_update['Seq_13'] == '' else [int(x) for x in str(r_update['Seq_13']).split(',')],
                          None if r_update['Seq_14'] == '' else [int(x) for x in str(r_update['Seq_14']).split(',')],
                          None if r_update['Seq_15'] == '' else [int(x) for x in str(r_update['Seq_15']).split(',')],
                          datetime.strptime(r_update['Edited'], "%d/%m/%Y %H:%M:%S"),
                          str(int(r_update['Now'] == 'TRUE'))])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Insert Irrigation Sequences: " + str(msg))
        return False


def automatic_days_get():
    try:
        command = "SELECT * FROM automatic_days"
        cursor_a.execute(command)
        records = cursor_a.fetchall()
        config.IRRIGATION_Days = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get Database Irrigation Automatic Days: " + str(msg))
        return False


def automatic_days_update(r_update):
    try:
        command = "UPDATE automatic_days SET " \
                  "days_name = %s, " \
                  "id_sequence = %s, " \
                  "monday = %s, tuesday = %s, wednesday = %s, thursday = %s, friday = %s, " \
                  "saturday = %s, sunday = %s, " \
                  "edited = %s " \
                  "WHERE id_garden = %s AND id_days = %s"

        cursor_a.execute(command,
                         [r_update['Days Name'], r_update['ID Sequence'],
                          None if r_update['Monday'] == '' else datetime.strptime(r_update['Monday'], "%H:%M:%S"),
                          None if r_update['Tuesday'] == '' else datetime.strptime(r_update['Tuesday'], "%H:%M:%S"),
                          None if r_update['Wednesday'] == '' else datetime.strptime(r_update['Wednesday'], "%H:%M:%S"),
                          None if r_update['Thursday'] == '' else datetime.strptime(r_update['Thursday'], "%H:%M:%S"),
                          None if r_update['Friday'] == '' else datetime.strptime(r_update['Friday'], "%H:%M:%S"),
                          None if r_update['Saturday'] == '' else datetime.strptime(r_update['Saturday'], "%H:%M:%S"),
                          None if r_update['Sunday'] == '' else datetime.strptime(r_update['Sunday'], "%H:%M:%S"),
                          datetime.strptime(r_update['Edited'], "%d/%m/%Y %H:%M:%S"),
                          r_update['ID Garden'], r_update['ID Days']])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Update Irrigation Sequences: " + str(msg))
        conn_a.rollback()
        return False


def automatic_days_insert(r_update):
    try:
        command = "INSERT INTO automatic_days " \
                  "(id_garden, id_days, days_name, id_sequence, monday, tuesday, wednesday, thursday, friday, " \
                  "saturday, sunday, edited) " \
                  "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor_a.execute(command,
                         [r_update['ID Garden'], r_update['ID Days'], r_update['Days Name'], r_update['ID Sequence'],
                          None if r_update['Monday'] == '' else datetime.strptime(r_update['Monday'], "%H:%M:%S"),
                          None if r_update['Tuesday'] == '' else datetime.strptime(r_update['Tuesday'], "%H:%M:%S"),
                          None if r_update['Wednesday'] == '' else datetime.strptime(r_update['Wednesday'], "%H:%M:%S"),
                          None if r_update['Thursday'] == '' else datetime.strptime(r_update['Thursday'], "%H:%M:%S"),
                          None if r_update['Friday'] == '' else datetime.strptime(r_update['Friday'], "%H:%M:%S"),
                          None if r_update['Saturday'] == '' else datetime.strptime(r_update['Saturday'], "%H:%M:%S"),
                          None if r_update['Sunday'] == '' else datetime.strptime(r_update['Sunday'], "%H:%M:%S"),
                          datetime.strptime(r_update['Edited'], "%d/%m/%Y %H:%M:%S")])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Insert Irrigation Sequences: " + str(msg))
        return False


def modes_get():
    try:
        command = "SELECT * FROM mode"
        cursor_a.execute(command)
        records = cursor_a.fetchall()
        config.GENERAL_Modes = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get Database Garden Configuration Modes: " + str(msg))
        return False


def modes_update(r_update):
    try:
        command = "UPDATE mode SET " \
                  "mode_description = %s, " \
                  "lcd = %s " \
                  "WHERE id_mode = %s"

        cursor_a.execute(command,
                         [r_update['Mode Description'], r_update['LCD'], r_update['ID Mode']])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Update Irrigation Sequences: " + str(msg))
        conn_a.rollback()
        return False


def modes_insert(r_update):
    try:
        command = "INSERT INTO mode " \
                  "(mode_description, lcd, id_mode) " \
                  "VALUES (%s, %s, %s)"
        cursor_a.execute(command,
                         [r_update['Mode Description'], r_update['LCD'], r_update['ID Mode']])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Insert Irrigation Sequences: " + str(msg))
        return False


def configuration_get():
    try:
        command = "SELECT * FROM config"
        cursor_a.execute(command)
        records = cursor_a.fetchall()
        config.GENERAL_Configuration = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get Database Garden General Configuration: " + str(msg))
        return False


def configuration_update(r_update):
    try:
        command = "UPDATE config SET " \
                  "id_config = %s, id_mode= %s, rain_sensor = %s, moisture_sensor = %s, humidity_level = %s, " \
                  "irrigation_cancel = %s, irrigation_cancel_day = %s, irrigation_now = %s, " \
                  "default_irrigation_sequence = %s, edited = %s " \
                  "WHERE id_garden = %s"

        cursor_a.execute(command,
                         [r_update['ID Config'], r_update['ID Mode'],
                          str(int(r_update['Rain Sensor'] == 'TRUE')), str(int(r_update['Moisture Sensor'] == 'TRUE')),
                          r_update['Humidity Level'], str(int(r_update['Irrigation Cancel'] == 'TRUE')),
                          datetime.strptime(r_update['Irrigation Cancel Day'], "%d/%m/%Y"),
                          str(int(r_update['Irrigation Now'] == 'TRUE')), r_update['Default Irrigation Sequence'],
                          datetime.strptime(r_update['Edited'], "%d/%m/%Y %H:%M:%S"),
                          r_update['ID Garden']])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Update Irrigation Sequences: " + str(msg))
        conn_a.rollback()
        return False


def configuration_insert(r_update):
    try:
        command = "INSERT INTO config " \
                  "(id_config, id_mode, rain_sensor, moisture_sensor, humidity_level, " \
                  "irrigation_cancel, irrigation_cancel_day, irrigation_now, " \
                  "default_irrigation_sequence, edited, id_garden)" \
                  "VALUES " \
                  "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        cursor_a.execute(command,
                         [r_update['ID Config'], r_update['ID Mode'],
                          str(int(r_update['Rain Sensor'] == 'TRUE')), str(int(r_update['Moisture Sensor'] == 'TRUE')),
                          r_update['Humidity Level'], str(int(r_update['Irrigation Cancel'] == 'TRUE')),
                          datetime.strptime(r_update['Irrigation Cancel Day'], "%d/%m/%Y"),
                          str(int(r_update['Irrigation Now'] == 'TRUE')), r_update['Default Irrigation Sequence'],
                          datetime.strptime(r_update['Edited'], "%d/%m/%Y %H:%M:%S"),
                          r_update['ID Garden']])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Update Irrigation Sequences: " + str(msg))
        conn_a.rollback()
        return False


def log_event(id_event, msg, value_log):
    try:
        command = ("INSERT INTO log_events "
                   "(datetime_log, id_garden, id_event, description, value_log, transfered) "
                   "VALUES (%s, %s, %s, %s, %s, %s)")
        cursor_a.execute(command, (datetime.now(), str(config.GARDEN_ID_Local), id_event, msg, value_log, '0'))
    except Exception, msg:
        logger.error("Saving event: {0}".format(msg))
    else:
        logger.debug("Log_event: [ID_Garden: {0}]: [Message: {1}: {2}]".format(
            config.GARDEN_ID_Local, id_event, msg, value_log))


def get_garden_information():
    try:
        command = "SELECT * FROM gardens WHERE id_garden = %s"
        cursor_a.execute(command, [str(config.GARDEN_ID_Local)])
        records = cursor_a.fetchone()
        config.GARDEN_Info = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get database config: " + str(msg))
        return False


def get_config():
    try:
        command = "SELECT config.*, mode.mode_description, mode.lcd as mode_lcd FROM config " \
                  "LEFT JOIN mode ON config.id_mode = mode.id_mode " \
                  "WHERE config.id_garden = %s"
        cursor_a.execute(command, [str(config.GARDEN_ID_Local)])
        records = cursor_a.fetchone()
        config.GARDEN_Config = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get database config: " + str(msg))
        return False


def get_sequences():
    try:
        command = "SELECT * FROM sequences WHERE id_garden = %s"
        cursor_a.execute(command, [str(config.GARDEN_ID_Local)])
        records = cursor_a.fetchall()
        config.GARDEN_Irrigation_Sequences = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get irrigation sequences: " + str(msg))
        return False


def get_irrigation_circuits():
    try:
        command = "SELECT * FROM irrigation_circuits WHERE id_garden = %s"
        cursor_a.execute(command, [str(config.GARDEN_ID_Local)])
        records = cursor_a.fetchall()
        config.GARDEN_Irrigation_Circuits = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get irrigation sequences: " + str(msg))
        return False


def get_irrigation_days():
    try:
        command = "SELECT * FROM automatic_days WHERE id_garden = %s"
        cursor_a.execute(command, [str(config.GARDEN_ID_Local)])
        records = cursor_a.fetchall()
        config.GARDEN_Irrigation_Days = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get irrigation sequences: " + str(msg))
        return False


# ----------------------------------------------------------------------------------------------------------------------
# MANUAL IRRIGATION
def update_manual_irrigation_on(_sequence):
    try:
        command = "UPDATE sequences SET irrigation_now = '1' WHERE id_sequence = %s"
        cursor_a.execute(command, [_sequence])
        conn_a.commit()
        command = "UPDATE config SET irrigation_now = '1', edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, [datetime.now(), config.GARDEN_ID_Local])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-SET Manual Irrigation ON: " + str(msg))
        return False


def manual_irrigation_off():
    try:
        command = "UPDATE config SET irrigation_now = '0', edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, [datetime.now(), config.GARDEN_Id])
        conn_a.commit()
    except Exception, msg:
        logger.error("DB-SET Manual Irrigation OFF [Update Config]: " + str(msg))


def manual_irrigation_off_sequence():
    try:
        command = "UPDATE sequences SET irrigation_now = '0', edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, [datetime.now(), config.GARDEN_Id])
        conn_a.commit()
    except Exception, msg:
        logger.error("DB-SET Manual Irrigation OFF [Update Sequences]: " + str(msg))
# ----------------------------------------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------------------------------------
# CONFIGURATION - MODE OF OPERATION
def save_mode(id_mode):
    try:
        command = "UPDATE config SET id_mode = %s, edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, [id_mode, datetime.now(), config.GARDEN_ID_Local])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        conn_a.rollback()
        return False
# ----------------------------------------------------------------------------------------------------------------------


def save_rain_sensor(sensor):
    try:
        command = "UPDATE config SET rain_sensor = %s, edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, ['1' if sensor else '0', datetime.now(), config.GARDEN_ID_Local])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-SET Rain sensor value in Config: " + str(msg))
        conn_a.rollback()
        return False
# ----------------------------------------------------------------------------------------------------------------------


def save_moisture_sensor(sensor):
    try:
        command = "UPDATE config SET moisture_sensor = %s, edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, ['1' if sensor else '0', datetime.now(), config.GARDEN_ID_Local])
        conn_a.commit()
    except Exception, msg:
        logger.error("DB-SET Soil moisture sensor value in Config: " + str(msg))
        conn_a.rollback()
# ----------------------------------------------------------------------------------------------------------------------


def save_moisture_sensor_limit(level):
    try:
        command = "UPDATE config SET humidity_level = %s, edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, [level, datetime.now(), config.GARDEN_ID_Local])
        conn_a.commit()
    except Exception, msg:
        logger.error("DB-SET Soil moisture sensor value in Config: " + str(msg))
        conn_a.rollback()
# ----------------------------------------------------------------------------------------------------------------------


def save_irrigation_cancel(canceled, date_cancel):
    try:
        command = "UPDATE config SET irrigation_cancel = %s, irrigation_cancel_day = %s, " \
                  "edited = %s WHERE id_garden = %s"
        cursor_a.execute(command, ['1' if canceled else '0', datetime.strptime(date_cancel, "%d/%m/%Y"),
                                   datetime.now(), config.GARDEN_ID_Local])
        conn_a.commit()
    except Exception, msg:
        logger.error("DB-SET Irrigation Cancel value in Config: " + str(msg))
        conn_a.rollback()


def get_mode_name(id_mode):
    try:
        command = "SELECT * FROM mode WHERE id_mode = %s"
        cursor_a.execute(command, [id_mode])
        records = cursor_a.fetchone()
        conn_a.commit()
        return [records['mode_description'], records['lcd']]
    except Exception, msg:
        logger.error("DB-Get database config: " + str(msg))
        return [' ', ' ']


def get_gardens():
    try:
        command = "SELECT * FROM jardin"
        cursor_a.execute(command)
        records = cursor_a.fetchall()
        config.IRRIGATION_Gardens = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get database garden_id: " + str(msg))
        return False


def get_garden_id():
    try:
        command = "SELECT * FROM jardin WHERE id_jardin = %s"
        cursor_a.execute(command, [config.GARDEN_ID])
        records = cursor_a.fetchone()
        conn_a.commit()
        return [records['nombre_jardin'], records['lcd_jardin']]
    except Exception, msg:
        logger.error("DB-Get database garden_id: " + str(msg))
        return [' ', ' ']


def get_irrigation_time():
    try:
        command = "SELECT id_tiempo_riego FROM config"
        cursor_a.execute(command)
        records = cursor_a.fetchone()
        config.GENERAL_TiempoRiego = records['id_tiempo_riego']
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        return False


def save_event(id_jardin, fecha, mensaje):
    try:
        command = ("INSERT INTO eventos "
                   "(id_jardin, fechahora, mensaje) "
                   "VALUES (%s, %s, %s)")
        cursor_a.execute(command, (id_jardin, fecha, mensaje))
        command = "SELECT currval('eventos_id_seq')"
        cursor_a.execute(command)
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        logger.error("Saving event: " + exc_type.__name__ + " - " + str(exc_obj) + " @"
                     + str(exc_tb.tb_lineno))
        raise
    else:
        logger.debug("Event saved: [id_jardin: {0}]: [mensaje: {1}]".format(id_jardin, mensaje))
        return cursor_a.fetchone()[0]


def get_sequences_old():
    try:
        command = "SELECT * FROM riego_sequencia WHERE id_jardin = %s"
        cursor_a.execute(command, [config.GARDEN_ID])
        records = cursor_a.fetchall()
        config.IRRIGATION_Sequence = records
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get irrigation sequences: " + str(msg))
        return False





def get_mode_all():
    try:
        command = "SELECT * FROM modo"
        cursor_a.execute(command)
        records = cursor_a.fetchall()
        conn_a.commit()
        return records
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        return None





def save_state(state):
    try:
        command = "UPDATE config SET estado = %s"
        cursor_a.execute(command, [state])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        return False


def save_time(new_time):
    try:
        command = "UPDATE config SET id_tiempo_riego = %s"
        cursor_a.execute(command, [new_time])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        return False


def save_sensibility_sensor(new_time):
    try:
        command = "UPDATE config SET id_sensibilidad_humedad = %s"
        cursor_a.execute(command, [new_time])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        return False


def get_manual_irrigation():
    try:
        command = "SELECT * FROM riego_manual WHERE id_jardin = %s"
        cursor_a.execute(command, [config.GARDEN_ID])
        records = cursor_a.fetchone()
        conn_a.commit()
        return records['riego_manual']
    except Exception, msg:
        logger.error("DB-Get irrigation sequences: " + str(msg))
        return False


def save_manual_irrigation_all():
    try:
        command = "UPDATE riego_manual SET riego_manual = '1'"
        cursor_a.execute(command)
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        return False


def update_manual_irrigation_off():
    try:
        command = "UPDATE riego_manual SET riego_manual = '0' WHERE id_jardin = %s"
        cursor_a.execute(command, [config.GARDEN_ID])
        conn_a.commit()
        return True
    except Exception, msg:
        logger.error("DB-Get databes config: " + str(msg))
        return False


