import logging

import forecastio
import config
from datetime import datetime, timedelta

api_key = 'd6fe144b40499bf7826f0d6b46de193d'
lat = config.Latitude
lng = config.Longitude

logger = logging.getLogger(__name__)


def get_forecast():
    weather = {'id': 0,
               'precipType': '',
               'precipIntensity': 0.0}
    try:
        logger.debug('      Forecast: [Latitude: {0}] [Longitude: {1}]'.format(lat, lng))
        forecast = forecastio.load_forecast(api_key, lat, lng)
        f1 = forecast.currently()
        f2 = forecast.daily()
        logger.debug('      Forecast: {0}'.format(f1))

        # summary
        weather['time'] = f1.time + timedelta(hours=-6)
        weather['summary'] = f1.summary
        weather['icon'] = f1.icon

        weather['temperature'] = f1.temperature
        weather['apparentTemperature'] = f1.apparentTemperature
        weather['humidity'] = f1.humidity
        weather['visibility'] = f1.visibility
        weather['ozone'] = f1.ozone

        weather['precipProbability'] = f1.precipProbability

        weather['dewPoint'] = f1.dewPoint  # punto de rocio
        weather['pressure'] = f1.pressure
        weather['windSpeed'] = f1.windSpeed
        weather['windGust'] = f1.windGust  # rafaga de viento
        weather['windBearing'] = f1.windBearing  # oscilacion de viento
        weather['cloudCover'] = f1.cloudCover
        weather['uvIndex'] = f1.uvIndex

        # daily
        for x in f2.data:
            if x.time.date() == datetime.now().date():
                weather['sunsetTime'] = x.sunsetTime + timedelta(hours=-6)
                weather['sunriseTime'] = x.sunriseTime + timedelta(hours=-6)
                weather['moonPhase'] = x.moonPhase
                weather['temperatureMin'] = x.temperatureMin
                weather['temperatureMinTime'] = x.temperatureMinTime # + timedelta(hours=-6)
                weather['temperatureMax'] = x.temperatureMax
                weather['temperatureMaxTime'] = x.temperatureMaxTime # + timedelta(hours=-6)
                weather['precipIntensityMax'] = x.precipIntensityMax

        weather['precipType'] = f1.precipType
        weather['precipIntensity'] = f1.precipIntensity
        # weather['precipIntensityError'] = f1.precipIntensityError

    except Exception, msg:
        logger.error("   ? Error: get forecast {0} ".format(str(msg)))
    finally:
        return weather

