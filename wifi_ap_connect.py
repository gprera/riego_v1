from wifi import Cell, Scheme
import logging.config
import logging
import os
from subprocess import check_output

# LOGGER
logUser = False


def change_dir():
    if getattr(sys, 'frozen', False):
        application_path = os.path.dirname(os.path.abspath(sys.executable))
    else:
        application_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(application_path)


def create_logger():
    logging.config.fileConfig("loggers.config")
    return logging.getLogger('wifi')


# ------------------------------------------------------------------------
# GET CONFIG
import config
import urllib2


def internet_on():
    try:
        urllib2.urlopen('http://www.google.com', timeout=1) # google url
        logger.debug("INTERNET connected")
        return True
    except urllib2.URLError as err:
        logger.error("No INTERNET connection by: {0}".format(err))
        return False


def get_ssid_connected():
    try:
        # scanoutput = check_output(["iwlist", "wlan0", "scan"])
        scanoutput = check_output(["iwconfig", "wlan0"])
        for line in scanoutput.split():
            if line.startswith("ESSID"):
                ssid = line.split('"')[1]
        logger.info("Wi-Fi connected in: {0}".format(ssid))
        return ssid
    except Exception as err:
        logger.error("Error getting the SSID Wireless by: {0}".format(err))
        return ''


if __name__ == '__main__':
    # change_dir()
    logger = create_logger()
    logger.info('-' * 50)
    logger.info('Wi-Fi: Connect automatically to Access Point')

    # Check if wi-fi is enabled in configuration
    if not config.WIFI_CONNECT:
        logger.info('Wi-Fi: Wifi is disable by configuration')
        internet_on()
        logger.info('-' * 50)
        exit(0)

    # Check if is connected in correct Access-Point
    actual_SSID = get_ssid_connected()
    logger.debug("Compare with:       {0}".format(config.SSID))
    if actual_SSID == config.SSID:
        internet_on()
        logger.info('-' * 50)
        exit(0)
    # Check the Access-Point detected in Range
    cells = Cell.all('wlan0')
    cell = None
    for cell_x in cells:
        logger.debug("Network: {0}".format(cell_x.ssid))
        if cell_x.ssid == config.SSID:
            cell = cell_x

    if cell is None:
        logger.error("Not {0} in wireless range".format(config.SSID))
        exit(0)

    scheme = Scheme.find('wlan0', cell.ssid)
    logger.debug("scheme exists: {0}".format(scheme))
    if scheme is None:
        scheme = Scheme.for_cell('wlan0', cell.ssid, cell, config.SSID_PASS)
        scheme.save()

    logger.debug("scheme activate: {0}".format(cell.ssid))
    scheme.activate()

    internet_on()
    logger.info('-' * 50)
